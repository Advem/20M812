
/*
Project: 20M812
Version: 1.7
Date: 01-02-2018
Author: Adam Advem Drabik
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>

/// Info
int app_version = 1;
int app_build = 7;
int app_update = 1;
int date_day = 01;
int date_month = 02;
int date_year = 2018;

/// Options
int music_on;
int sounds_on;
int animation_on;
int enemy_hud_on;
int stats_hud_on;
int app_hud_on;
int game_hud_on;
int interface_on;
float music_volume;
float sound_volume;

/// Display
const int SCREEN_W = 1280;
const int SCREEN_H = 1024;

/// Random Generator
int source;
float random_z1x = 0;
float random_z2x = 0;
float random_z3x = 0;
float random_z4x = 0;
float random_z1y = 0;
float random_z2y = 0;
float random_z3y = 0;
float random_z4y = 0;
float random_h1x = 0;
float random_h2x = 0;
float random_h3x = 0;
float random_h4x = 0;
float random_h1y = 0;
float random_h2y = 0;
float random_h3y = 0;
float random_h4y = 0;

/// Enemy Spawning
int zombie_density = 1;
int human_density = 1;
int boss_density = 0;
float boss_time;

/// Characters Positioning
float hero_x = 100;
float hero_y = 512 - 200;

float boss_x = 1300;
float boss_y = 290; 
int boss_alive;

float zombie_x = 1300;
float zombie_y = 288;
int zombie_alive;

float zombie2_x = 1300;
float zombie2_y = 288;
int zombie2_alive;

float zombie3_x = 1300;
float zombie3_y = 288;
int zombie3_alive;

float zombie4_x = 1300;
float zombie4_y = 288;
int zombie4_alive;

float human_x = 1300;
float human_y = 288;
int human_alive;

float human2_x = 1300;
float human2_y = 288;
int human2_alive;

float human3_x = 1300;
float human3_y = 288;
int human3_alive;

float human4_x = 1300;
float human4_y = 288;
int human4_alive;

float fireball_x;
float fireball_y;
int fireball_alive;

/// Elements Positioning
float arrow_x;
float arrow_y;
int shoot_arrow = 0;

float heal_x;
float heal_y;
int shoot_heal = 0;

float ultimate_x;
float ultimate_y;
int shoot_ultimate = 2;
int cooldown_ultimate;

/// Characters Current State
float hero_current_hp;
float boss_current_hp;
float zombie_current_hp;
float zombie2_current_hp;
float zombie3_current_hp;
float zombie4_current_hp;
float human_current_hp;
float human2_current_hp;
float human3_current_hp;
float human4_current_hp;
float light_points_current;
float light_points_max;
float dark_points_current;
float dark_points_max;
float percent_hp;
float percent_boss_hp;
float percent_light_points;
float percent_dark_points;
float morality_cap = 400;

/// Level
int game_level;

/// Normal Hero
float nh_hp, nh_dmg, nh_spd, morality, nh_mvn;

/// Light Hero
float lh_hp, lh_dmg, lh_spd, lh_cd, lh_mvn;

/// Dark Hero
float dh_hp, dh_dmg, dh_spd, dh_cd, dh_mvn;

/// Zombie Enemy
float ze_hp, ze_dmg, ze_mvn;

/// Human Enemy
float he_hp, he_dmg, he_mvn;

/// Boss Enemy
float be_hp, be_dmg, be_mvn, be_spd;

/// Keys Load
enum MYKEYS
{
	KEY_W, KEY_S, KEY_A, KEY_D, KEY_J, KEY_K, KEY_L
};

FILE *options;
FILE *stats;
FILE *level;
FILE *spawn;
FILE *hero;
errno_t err;

bool save_options()
{
	err = fopen_s(&options, "options.txt", "w+");
	if (err == 0)
	{
		printf("Options saved to the text file.\n");
	}

	else
	{
		printf("The file 'options' has not been opened.\n");
		return false;
	}

	fprintf_s(options, "%i %i %i %i %i %i %i %i %.2f %.2f", music_on, sounds_on, animation_on, enemy_hud_on, stats_hud_on, app_hud_on, game_hud_on, interface_on, music_volume, sound_volume);
	//printf("%i %i %i %i %i %i %i %i %.2f\n", music_on, sounds_on, animation_on, enemy_hud_on, stats_hud_on, app_hud_on, game_hud_on, interface_on, music_volume, sound_volume);

	_fcloseall();
	return true;
}

bool load_options()
{
	err = fopen_s(&options, "options.txt", "r");
	if (err == 0)
	{
		printf("Options loaded from the text file.\n");
	}

	else
	{
		printf("The file 'options' has not been opened.\n");
		return false;
	}

	fscanf_s(options, "%i", &music_on);
	fscanf_s(options, "%i", &sounds_on);
	fscanf_s(options, "%i", &animation_on);
	fscanf_s(options, "%i", &enemy_hud_on);
	fscanf_s(options, "%i", &stats_hud_on);
	fscanf_s(options, "%i", &app_hud_on);
	fscanf_s(options, "%i", &game_hud_on);
	fscanf_s(options, "%i", &interface_on);
	fscanf_s(options, "%f", &music_volume);
	fscanf_s(options, "%f", &sound_volume);
	//printf(" music on = %i\n sounds on = %i\n animation on = %i\n curosr on = %i\n game hud on = %i\n app hud on = %i\n enemy hud on = %i\n interface on = %i\n\n", music_on, sounds_on, animation_on, enemy_hud_on, stats_hud_on, app_hud_on, game_hud_on, interface_on);

	_fcloseall();
	return true;
}

bool save_stats()
{
	err = fopen_s(&stats, "stats.txt", "w+");
	if (err == 0)
	{
		printf("Stats saved to the text file.\n");
	}

	else
	{
		printf("The file 'stats' has not been opened.\n");
		return false;
	}

	fprintf_s(stats, "%.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f", nh_hp, nh_dmg, nh_spd, morality, nh_mvn, lh_hp, lh_dmg, lh_spd, lh_cd, lh_mvn, dh_hp, dh_dmg, dh_spd, dh_cd, dh_mvn, ze_hp, ze_dmg, ze_mvn, he_hp, he_dmg, he_mvn, be_hp, be_dmg, be_mvn);
	//printf("%.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f\n", nh_hp, nh_dmg, nh_spd, morality, nh_mvn, lh_hp, lh_dmg, lh_spd, lh_cd, lh_mvn, dh_hp, dh_dmg, dh_spd, dh_cd, dh_mvn, ze_hp, ze_dmg, ze_mvn, he_hp, he_dmg, he_mvn, be_hp, be_dmg, be_mvn);

	_fcloseall();
	return true;
}

bool load_stats()
{
	err = fopen_s(&stats, "stats.txt", "r");
	if (err == 0)
	{
		printf("Stats loaded from the text file.\n");
	}

	else
	{
		printf("The file 'stats' has not been opened.\n");
		return false;
	}

	fscanf_s(options, "%f", &nh_hp);
	fscanf_s(options, "%f", &nh_dmg);
	fscanf_s(options, "%f", &nh_spd);
	fscanf_s(options, "%f", &morality);
	fscanf_s(options, "%f", &nh_mvn);
	fscanf_s(options, "%f", &lh_hp);
	fscanf_s(options, "%f", &lh_dmg);
	fscanf_s(options, "%f", &lh_spd);
	fscanf_s(options, "%f", &lh_cd);
	fscanf_s(options, "%f", &lh_mvn);
	fscanf_s(options, "%f", &dh_hp);
	fscanf_s(options, "%f", &dh_dmg);
	fscanf_s(options, "%f", &dh_spd);
	fscanf_s(options, "%f", &dh_cd);
	fscanf_s(options, "%f", &dh_mvn);
	fscanf_s(options, "%f", &ze_hp);
	fscanf_s(options, "%f", &ze_dmg);
	fscanf_s(options, "%f", &ze_mvn);
	fscanf_s(options, "%f", &he_hp);
	fscanf_s(options, "%f", &he_dmg);
	fscanf_s(options, "%f", &he_mvn);
	fscanf_s(options, "%f", &be_hp);
	fscanf_s(options, "%f", &be_dmg);
	fscanf_s(options, "%f", &be_mvn);
	//printf(" normal hero health = %.1f\n normal hero damage = %.1f\n normal hero speed = %.1f\n normal hero cooldown = %.1f\n normal hero movement = %.1f\n light hero health = %.1f\n light hero damage = %.1f\n light hero speed = %.1f\n light hero cooldown = %.1f\n light hero movement = %.1f\n dark hero health = %.1f\n dark hero damage = %.1f\n dark hero speed = %.1f\n dark hero cooldown = %.1f\n normal dark movement = %.1f\n zombie enemy health = %.1f\n zombie enemy damage = %.1f\n zombie enemy movement = %.1f\n human enemy health = %.1f\n human enemy damage = %.1f\n human enemy movement = %.1f\n\n", nh_hp, nh_dmg, nh_spd, nh_cd, nh_mvn, lh_hp, lh_dmg, lh_spd, lh_cd, lh_mvn, dh_hp, dh_dmg, dh_spd, dh_cd, dh_mvn, ze_hp, ze_dmg, ze_mvn, he_hp, he_dmg, he_mvn);

	_fcloseall();
	return true;
}

bool save_spawn()
{
	err = fopen_s(&spawn, "spawn.txt", "w+");
	if (err == 0)
	{
		printf("Spawn saved to the text file..\n");
	}

	else
	{
		printf("The file 'spawn' has not been opened.\n");
		return false;
	}

	fprintf_s(spawn, "%i %i %i", zombie_density, human_density, boss_density);
	//printf("%i %i %i\n", zombie_density, human_density, boss_density);

	_fcloseall();
	return true;
}

bool load_spawn()
{
	err = fopen_s(&spawn, "spawn.txt", "r");
	if (err == 0)
	{
		printf("Spawn loaded from the text file.\n");
	}

	else
	{
		printf("The file 'spawn' has not been opened.\n");
		return false;
	}

	fscanf_s(spawn, "%i", &zombie_density);
	fscanf_s(spawn, "%i", &human_density);
	fscanf_s(spawn, "%i", &boss_density);
	//printf(" level = %i\n\n", game_level);

	_fcloseall();
	return true;
}

bool save_level()
{
	err = fopen_s(&level, "level.txt", "w+");
	if (err == 0)
	{
		printf("Level saved to the text file.\n");
	}

	else
	{
		printf("The file 'level' has not been opened.\n");
		return false;
	}

	fprintf_s(level, "%i", game_level);
	//printf("%i\n", game_level);

	_fcloseall();
	return true;
}

bool load_level()
{
	err = fopen_s(&level, "level.txt", "r");
	if (err == 0)
	{
		printf("Level loaded from the text file.\n");
	}

	else
	{
		printf("The file 'level' has not been opened.\n");
		return false;
	}

	fscanf_s(level, "%i", &game_level);
	//printf(" level = %i\n\n", game_level);

	_fcloseall();
	return true;
}

bool save_hero()
{
	err = fopen_s(&hero, "hero.txt", "w+");
	if (err == 0)
	{
		printf("Hero saved to the text file.\n");
	}

	else
	{
		printf("The file 'hero' has not been opened.\n");
		return false;
	}

	fprintf_s(hero, "%0.f %0.f %0.f %0.f %0.f", hero_current_hp, light_points_current, light_points_max, dark_points_current, dark_points_max);
	//printf("%0.f %0.f %0.f %0.f %0.f", hero_current_hp, light_points_current, light_points_max, dark_points_current, dark_points_max);

	_fcloseall();
	return true;
}

bool load_hero()
{
	err = fopen_s(&hero, "hero.txt", "r");
	if (err == 0)
	{
		printf("Hero loaded from the text file.\n");
	}

	else
	{
		printf("The file 'hero' has not been opened.\n");
		return false;
	}

	fscanf_s(hero, "%f", &hero_current_hp);
	fscanf_s(hero, "%f", &light_points_current);
	fscanf_s(hero, "%f", &light_points_max);
	fscanf_s(hero, "%f", &dark_points_current);
	fscanf_s(hero, "%f", &dark_points_max);
	//printf(" music on = %i\n sounds on = %i\n animation on = %i\n curosr on = %i\n game hud on = %i\n app hud on = %i\n enemy hud on = %i\n interface on = %i\n\n", music_on, sounds_on, animation_on, enemy_hud_on, stats_hud_on, app_hud_on, game_hud_on, interface_on);

	_fcloseall();
	return true;
}

void arrow()
{
	if (arrow_x < SCREEN_W && shoot_arrow == 1 /*|| arrow_y >= zombie_y && arrow_y <= zombie_y + 151 && shoot_arrow == 1*/)
	{
		shoot_arrow = 1;
		if (light_points_current < light_points_max && dark_points_current < dark_points_max) arrow_x += nh_spd;
		if (light_points_current >= light_points_max) arrow_x += lh_spd;
		if (dark_points_current >= dark_points_max) arrow_x += dh_spd;	
	}	

	else
	{
		shoot_arrow = 0;
	}
}

void heal()
{
	if (heal_x < SCREEN_W && shoot_heal == 1)
	{
		shoot_heal = 1;
		if (light_points_current < light_points_max && dark_points_current < dark_points_max) heal_x += nh_spd;
		if (light_points_current >= light_points_max) heal_x += lh_spd;
		if (dark_points_current >= dark_points_max) heal_x += dh_spd;
	}

	else
	{
		shoot_heal = 0;
	}
}

void ultimate()
{
	if (hero_x < SCREEN_W && shoot_ultimate == 1)
	{
		if (light_points_current >= light_points_max) shoot_ultimate = 1;
		if (dark_points_current >= dark_points_max) shoot_ultimate = 1;
		
	}

	else
	{
		shoot_ultimate = 2;
	}
}

void zombie_spawn()
{
		if (zombie_density >= 1)
	{
		if (zombie_x >= -85 && zombie_alive == 1 && zombie_current_hp > 0) 
		{
			zombie_alive = 1;
			if (zombie_current_hp < ze_hp) zombie_x -= ze_mvn / 2;
			if (zombie_current_hp >= ze_hp) zombie_x -= ze_mvn;
			if (zombie_x <= -66 && zombie_x > -67 && zombie_current_hp < ze_hp) hero_current_hp -= ze_dmg;
		}

		if (zombie_x >= -85 && zombie_alive == 1 && zombie_current_hp <= 0) 
		{
			zombie_alive = 1;
			zombie_x -= ze_mvn;
		}


		else if (zombie_x <= -85)//|| zombie_current_hp <= 0) // if killed tp to 1300
		{
			zombie_alive = 0;
			zombie_current_hp = ze_hp / 2; 
		}
	}

		if (zombie_density >= 2)
		{
			if (zombie2_x >= -85 && zombie2_alive == 1 && zombie2_current_hp > 0) 
			{
				zombie2_alive = 1;
				if (zombie2_current_hp < ze_hp) zombie2_x -= ze_mvn / 2;
				if (zombie2_current_hp >= ze_hp) zombie2_x -= ze_mvn;
				if (zombie2_x <= -66 && zombie2_x > -67 && zombie2_current_hp < ze_hp) hero_current_hp -= ze_dmg;
			}

			if (zombie2_x >= -85 && zombie2_alive == 1 && zombie2_current_hp <= 0) 
			{
				zombie2_alive = 1;
				zombie2_x -= ze_mvn;
			}


			else if (zombie2_x <= -85)//|| zombie2_current_hp <= 0) // if killed tp to 1300
			{
				zombie2_alive = 0;
				zombie2_current_hp = ze_hp / 2; 
			}
		}

		if (zombie_density >= 3)
		{
			if (zombie3_x >= -85 && zombie3_alive == 1 && zombie3_current_hp > 0)
			{
				zombie3_alive = 1;
				if (zombie3_current_hp < ze_hp) zombie3_x -= ze_mvn / 2;
				if (zombie3_current_hp >= ze_hp) zombie3_x -= ze_mvn;
				if (zombie3_x <= -66 && zombie3_x > -67 && zombie3_current_hp < ze_hp) hero_current_hp -= ze_dmg;
			}

			if (zombie3_x >= -85 && zombie3_alive == 1 && zombie3_current_hp <= 0)
			{
				zombie3_alive = 1;
				zombie3_x -= ze_mvn;
			}


			else if (zombie3_x <= -85)//|| zombie3_current_hp <= 0) // if killed tp to 1300
			{
				zombie3_alive = 0;
				zombie3_current_hp = ze_hp / 2;
			}
		}

		if (zombie_density >= 4)
		{
			if (zombie4_x >= -85 && zombie4_alive == 1 && zombie4_current_hp > 0)
			{
				zombie4_alive = 1;
				if (zombie4_current_hp < ze_hp) zombie4_x -= ze_mvn / 2;
				if (zombie4_current_hp >= ze_hp) zombie4_x -= ze_mvn;
				if (zombie4_x <= -66 && zombie4_x > -67 && zombie4_current_hp < ze_hp) hero_current_hp -= ze_dmg;
			}

			if (zombie4_x >= -85 && zombie4_alive == 1 && zombie4_current_hp <= 0)
			{
				zombie4_alive = 1;
				zombie4_x -= ze_mvn;
			}


			else if (zombie4_x <= -85)//|| zombie4_current_hp <= 0) // if killed tp to 1300
			{
				zombie4_alive = 0;
				zombie4_current_hp = ze_hp / 2;
			}
		}

}

void human_spawn()
{
	if (human_density >= 1)
	{
		if (human_x >= -85 && human_alive == 1 && human_current_hp > 0)
		{
			human_alive = 1;
			if (human_current_hp < he_hp) human_x -= he_mvn / 2;
			if (human_current_hp >= he_hp) human_x -= he_mvn;
			if (human_x <= -66 && human_x > -67 && human_current_hp < he_hp) hero_current_hp -= he_dmg;
		}

		if (human_x >= -85 && human_alive == 1 && human_current_hp <= 0)
		{
			human_alive = 1;
			human_x -= he_mvn;
		}


		else if (human_x <= -85)//|| human_current_hp <= 0) // if killed tp to 1300
		{
			human_alive = 0;
			human_current_hp = he_hp / 2;
		}
	}

	if (human_density >= 2)
	{
		if (human2_x >= -85 && human2_alive == 1 && human2_current_hp > 0)
		{
			human2_alive = 1;
			if (human2_current_hp < he_hp) human2_x -= he_mvn / 2;
			if (human2_current_hp >= he_hp) human2_x -= he_mvn;
			if (human2_x <= -66 && human2_x > -67 && human2_current_hp < he_hp) hero_current_hp -= he_dmg;
		}

		if (human2_x >= -85 && human2_alive == 1 && human2_current_hp <= 0)
		{
			human2_alive = 1;
			human2_x -= he_mvn;
		}


		else if (human2_x <= -85)//|| human2_current_hp <= 0) // if killed tp to 1300
		{
			human2_alive = 0;
			human2_current_hp = he_hp / 2;
		}
	}

	if (human_density >= 3)
	{
		if (human3_x >= -85 && human3_alive == 1 && human3_current_hp > 0)
		{
			human3_alive = 1;
			if (human3_current_hp < he_hp) human3_x -= he_mvn / 2;
			if (human3_current_hp >= he_hp) human3_x -= he_mvn;
			if (human3_x <= -66 && human3_x > -67 && human3_current_hp < he_hp) hero_current_hp -= he_dmg;
		}

		if (human3_x >= -85 && human3_alive == 1 && human3_current_hp <= 0)
		{
			human3_alive = 1;
			human3_x -= he_mvn;
		}


		else if (human3_x <= -85)//|| human3_current_hp <= 0) // if killed tp to 1300
		{
			human3_alive = 0;
			human3_current_hp = he_hp / 2;
		}
	}

	if (human_density >= 4)
	{
		if (human4_x >= -85 && human4_alive == 1 && human4_current_hp > 0)
		{
			human4_alive = 1;
			if (human4_current_hp < he_hp) human4_x -= he_mvn / 2;
			if (human4_current_hp >= he_hp) human4_x -= he_mvn;
			if (human4_x <= -66 && human4_x > -67 && human4_current_hp < he_hp) hero_current_hp -= he_dmg;
		} 

		if (human4_x >= -85 && human4_alive == 1 && human4_current_hp <= 0)
		{
			human4_alive = 1;
			human4_x -= he_mvn;
		}


		else if (human4_x <= -85)//|| human4_current_hp <= 0) // if killed tp to 1300
		{
			human4_alive = 0;
			human4_current_hp = he_hp / 2;
		}
	}
}

void boss_spawn()
{
	if (boss_density >= 1)
	{
		if (boss_x >= -85 && boss_alive == 1 && boss_current_hp > 0)
		{
			boss_alive = 1;
			if (boss_current_hp < be_hp) boss_x -= be_mvn / 2;
			if (boss_current_hp >= be_hp) boss_x -= be_mvn;
			if (boss_x <= -66 && boss_x > -67 && boss_current_hp < be_hp) boss_current_hp -= be_dmg;
		}

		if (boss_x >= -85 && boss_alive == 1 && boss_current_hp <= 0)
		{
			boss_alive = 1;
			boss_x -= he_mvn;
		}

		else if (boss_x <= -85)//|| boss_current_hp <= 0) // if killed tp to 1300
		{
			boss_alive = 0;
		}
	}
}

void fireball_spawn()
{
	if (boss_density >= 1)
	{
		if (fireball_x > -50 && fireball_x < SCREEN_W + 50 && fireball_y < SCREEN_H + 50 && fireball_y > -50 && fireball_alive == 1 && boss_current_hp > 0 && boss_alive == 1)
		{
			fireball_alive = 1;
			
			
			if (boss_x >= hero_x - 200) fireball_y -= 1 / (hero_x - 200 - boss_x) * be_spd; 
			if (boss_x >= hero_x - 200) fireball_x  -= 1 / (hero_y - 100 - boss_y) * be_spd;

			if (boss_x < hero_x - 200) fireball_y += 1 / (hero_x - 200 - boss_x) * be_spd;
			if (boss_x < hero_x - 200) fireball_x += 1 / (hero_y - 100 -boss_y) * be_spd;
			
		}

		else if (fireball_x <= -50 || fireball_x >= SCREEN_W + 50 || fireball_y >= SCREEN_H + 50 || fireball_y <= -50)
		{
			fireball_alive = 0;
			fireball_x = boss_x + 200;
			fireball_y = boss_y + 100;
		}
	}

}

void reset_stats()
{
	/// Stats Setting (stats.txt)
	nh_hp = 10;
	nh_dmg = 1;
	nh_spd = 10;
	nh_mvn = 1.5;
	morality = 6;

	lh_hp = 20;
	lh_dmg = 2;
	lh_spd = 12;
	lh_cd = 30;
	lh_mvn = 2.0;

	dh_hp = 20;
	dh_dmg = 2;
	dh_spd = 12;
	dh_cd = 30;
	dh_mvn = 2.0;

	ze_hp = 6;
	ze_dmg = 2;
	ze_mvn = 1.5;

	he_hp = 6;
	he_dmg = 2;
	he_mvn = 1.8;

	be_hp = 100;
	be_dmg = 1;
	be_mvn = 0.6;
	be_spd = 1000;

	boss_time = 60;

	 /// Hero and Enemy HP Setting
	hero_current_hp = nh_hp;
	zombie_current_hp = ze_hp / 2;
	zombie2_current_hp = ze_hp / 2;
	zombie3_current_hp = ze_hp / 2;
	zombie4_current_hp = ze_hp / 2;
	human_current_hp = he_hp / 2;
	human2_current_hp = he_hp / 2;
	human3_current_hp = he_hp / 2;
	human4_current_hp = he_hp / 2;
	boss_current_hp = be_hp / 2;

	/// Morality Setting (hero.txt)
	light_points_current = 0;
	light_points_max = 400;
	dark_points_current = 0;
	dark_points_max = 400;
	morality_cap = 400;
}

void reset_enemy_position()
{
	random_z1x = rand() % (1);
	random_z2x = rand() % (430);
	random_z3x = rand() % (430 + 430);
	random_z4x = rand() % (430 + 430 + 430);
	random_h1x = rand() % (1);
	random_h2x = rand() % (430);
	random_h3x = rand() % (430 + 430);
	random_h4x = rand() % (430 + 430 + 430);
	random_z1y = rand() % (1280 - 288 - 151 - 251 - 4);
	random_z2y = rand() % (1280 - 288 - 151 - 251 - 4);
	random_z3y = rand() % (1280 - 288 - 151 - 251 - 4);
	random_z4y = rand() % (1280 - 288 - 151 - 251 - 4);
	random_h1y = rand() % (1280 - 288 - 151 - 251 - 4);
	random_h2y = rand() % (1280 - 288 - 151 - 251 - 4);
	random_h3y = rand() % (1280 - 288 - 151 - 251 - 4);
	random_h4y = rand() % (1280 - 288 - 151 - 251 - 4);

	zombie_x = random_z1x + 1300;
	zombie_y = random_z1y + 288;
	zombie2_x = random_z2x + 1300 + 320;
	zombie2_y = random_z2y + 288;
	zombie3_x = random_z3x + 1300 + 320 + 320;
	zombie3_y = random_z3y + 288;
	zombie4_x = random_z4x + 1300 + 320 + 320 + 320;
	zombie4_y = random_z4y + 288;
	human_x = random_h1x + 1300;
	human_y = random_h1y + 288;
	human2_x = random_h2x + 1300 + 320;
	human2_y = random_h2y + 288;
	human3_x = random_h3x + 1300 + 320 + 320;
	human3_y = random_h3y + 288;
	human4_x = random_h4x + 1300 + 320 + 320 + 320;
	human4_y = random_h4y + 288;

	hero_x = 10;
	hero_y = 300;

	boss_x = 1300;
	boss_y = 50;

	shoot_arrow = 0;
	shoot_heal = 0;
	shoot_ultimate = 0;
}

void reset_health_points()
{
	zombie_current_hp = ze_hp / 2;
	zombie2_current_hp = ze_hp / 2;
	zombie3_current_hp = ze_hp / 2;
	zombie4_current_hp = ze_hp / 2;
	human_current_hp = he_hp / 2;
	human2_current_hp = he_hp / 2;
	human3_current_hp = he_hp / 2;
	human4_current_hp = he_hp / 2;
	boss_current_hp = be_hp / 2;
}

void set_stats()
{
	/// Level 2
	if (game_level == 2)
	{
		nh_hp = 10;
		nh_dmg = 1;
		nh_spd = 10;
		nh_mvn = 1.5;
		morality = 2;

		lh_hp = 10;
		lh_dmg = 2;
		lh_spd = 11;
		lh_cd = 30;
		lh_mvn = 2.0;

		dh_hp = 10;
		dh_dmg = 2;
		dh_spd = 11;
		dh_cd = 30;
		dh_mvn = 2.0;

		ze_hp = 8;
		ze_dmg = 2;
		ze_mvn = 1.5 + 0.2;

		he_hp = 8;
		he_dmg = 2;
		he_mvn = 1.8 + 0.2;

		be_hp = 150;
		be_dmg = 1;
		be_mvn = 0.7;
		be_spd = 1500;

		boss_time = 60;

		//light_points_current -= light_points_current * 0.3;
		//dark_points_current -= dark_points_current * 0.3;
		morality_cap = 400;
	}

	/// Level 3
	if (game_level == 3)
	{
		nh_hp = 15;
		nh_dmg = 1;
		nh_spd = 10;
		nh_mvn = 1.5;
		morality = 2;

		lh_hp = 15;
		lh_dmg = 2;
		lh_spd = 11;
		lh_cd = 25;
		lh_mvn = 2.0;

		dh_hp = 15;
		dh_dmg = 2;
		dh_spd = 11;
		dh_cd = 25;
		dh_mvn = 2.0;

		ze_hp = 10;
		ze_dmg = 3;
		ze_mvn = 1.5 + 0.3;

		he_hp = 10;
		he_dmg = 3;
		he_mvn = 1.8 + 0.3;

		be_hp = 200;
		be_dmg = 2;
		be_mvn = 0.8;
		be_spd = 2000;

		boss_time = 90;

		//light_points_current -= light_points_current * 0.3;
		//dark_points_current -= dark_points_current * 0.3;
		morality_cap = 400;
	}

	/// Level 4
	if (game_level == 4)
	{
		nh_hp = 15;
		nh_dmg = 1;
		nh_spd = 10;
		nh_mvn = 1.5;
		morality = 2;

		lh_hp = 15;
		lh_dmg = 2;
		lh_spd = 11;
		lh_cd = 25;
		lh_mvn = 2.0;

		dh_hp = 15;
		dh_dmg = 2;
		dh_spd = 11;
		dh_cd = 25;
		dh_mvn = 2.0;

		ze_hp = 12;
		ze_dmg = 3;
		ze_mvn = 1.5 + 0.4;

		he_hp = 12;
		he_dmg = 3;
		he_mvn = 1.8 + 0.4;

		be_hp = 220;
		be_dmg = 2;
		be_mvn = 0.8 + 0.1;
		be_spd = 2000;

		boss_time = 90;

		//light_points_current -= light_points_current * 0.3;
		//dark_points_current -= dark_points_current * 0.3;
		morality_cap = 400;
	}

	/// Level 5
	if (game_level == 5)
	{
		nh_hp = 20;
		nh_dmg = 2;
		nh_spd = 10;
		nh_mvn = 1.5;
		morality = 2;

		lh_hp = 20;
		lh_dmg = 3;
		lh_spd = 12;
		lh_cd = 20;
		lh_mvn = 2.0 + 0.2;

		dh_hp = 20;
		dh_dmg = 3;
		dh_spd = 12;
		dh_cd = 20;
		dh_mvn = 2.0 + 0.2;

		ze_hp = 16;
		ze_dmg = 4;
		ze_mvn = 1.5 + 0.5;

		he_hp = 16;
		he_dmg = 4;
		he_mvn = 1.8 + 0.5;

		be_hp = 250;
		be_dmg = 3;
		be_mvn = 0.8 + 0.1 + 0.1;
		be_spd = 2500;

		boss_time = 120;

		light_points_current -= light_points_current * 0.3;
		dark_points_current -= dark_points_current * 0.3;
		morality_cap = 500;
	}

	/// Level 6
	if (game_level == 6)
	{
		nh_hp = 20;
		nh_dmg = 2;
		nh_spd = 10;
		nh_mvn = 1.5;
		morality = 2;

		lh_hp = 20;
		lh_dmg = 3;
		lh_spd = 12;
		lh_cd = 20;
		lh_mvn = 2.0 + 0.2;

		dh_hp = 20;
		dh_dmg = 3;
		dh_spd = 12;
		dh_cd = 20;
		dh_mvn = 2.0 + 0.2;

		ze_hp = 18;
		ze_dmg = 4;
		ze_mvn = 1.5 + 0.6;

		he_hp = 18;
		he_dmg = 4;
		he_mvn = 1.8 + 0.6;

		be_hp = 300;
		be_dmg = 3;
		be_mvn = 0.8 + 0.1 + 0.1;
		be_spd = 2500;

		boss_time = 120;

		light_points_current -= light_points_current * 0.3;
		dark_points_current -= dark_points_current * 0.3;
		morality_cap = 500;
	}

	/// Level 7
	if (game_level == 7)
	{
		nh_hp = 25;
		nh_dmg = 2;
		nh_spd = 10;
		nh_mvn = 1.5 + 0.2;
		morality = 3;

		lh_hp = 25;
		lh_dmg = 5;
		lh_spd = 12 + 1;
		lh_cd = 15;
		lh_mvn = 2.0 + 0.4;

		dh_hp = 25;
		dh_dmg = 5;
		dh_spd = 12 + 1;
		dh_cd = 15;
		dh_mvn = 2.0 + 0.4;

		ze_hp = 40;
		ze_dmg = 5;
		ze_mvn = 1.5 + 0.7;

		he_hp = 40;
		he_dmg = 5;
		he_mvn = 1.8 + 0.7;

		be_hp = 500;
		be_dmg = 4;
		be_mvn = 0.8 + 0.1 + 0.1 + 0.1;
		be_spd = 3000;

		boss_time = 180;

		light_points_current -= light_points_current * 0.4;
		dark_points_current -= dark_points_current * 0.4;
		morality_cap = 600;
	}

	/// Level 8
	if (game_level == 8)
	{
		nh_hp = 25;
		nh_dmg = 2;
		nh_spd = 10;
		nh_mvn = 1.5 + 0.2;
		morality = 3;

		lh_hp = 25;
		lh_dmg = 5;
		lh_spd = 12 + 1;
		lh_cd = 15;
		lh_mvn = 2.0 + 0.4;

		dh_hp = 25;
		dh_dmg = 5;
		dh_spd = 12 + 1;
		dh_cd = 15;
		dh_mvn = 2.0 + 0.4;

		ze_hp = 50;
		ze_dmg = 6;
		ze_mvn = 1.5 + 0.8;

		he_hp = 50;
		he_dmg = 6;
		he_mvn = 1.8 + 0.8;

		be_hp = 600;
		be_dmg = 4;
		be_mvn = 0.8 + 0.1 + 0.1 + 0.1 + 0.1;
		be_spd = 3000;

		boss_time = 180;

		light_points_current -= light_points_current * 0.4;
		dark_points_current -= dark_points_current * 0.4;
		morality_cap = 600;
	}

	/// Level 9
	if (game_level == 9)
	{
		nh_hp = 30;
		nh_dmg = 4;
		nh_spd = 11;
		nh_mvn = 1.5 + 0.4;
		morality = 1;

		lh_hp = 30;
		lh_dmg = 6;
		lh_spd = 12 + 1;
		lh_cd = 10;
		lh_mvn = 2.0 + 0.6;

		dh_hp = 30;
		dh_dmg = 6;
		dh_spd = 12 + 1;
		dh_cd = 10;
		dh_mvn = 2.0 + 0.6;

		ze_hp = 60;
		ze_dmg = 8;
		ze_mvn = 1.5 + 0.9;

		he_hp = 60;
		he_dmg = 8;
		he_mvn = 1.8 + 0.9;

		be_hp = 800;
		be_dmg = 5;
		be_mvn = 0.8 + 0.1 + 0.1 + 0.1 + 0.1;
		be_spd = 3500;

		boss_time = 300;

		light_points_current -= light_points_current * 0.5;
		dark_points_current -= dark_points_current * 0.5;

		morality_cap = 700;
	}

	/// Level 10
	if (game_level == 10)
	{
		nh_hp = 40;
		nh_dmg = 5;
		nh_spd = 12;
		nh_mvn = 1.5 + 0.4;
		morality = 1;

		lh_hp = 40;
		lh_dmg = 10;
		lh_spd = 12 + 2 + 1;
		lh_cd = 5;
		lh_mvn = 2.0 + 0.6 + 0.1;

		dh_hp = 40;
		dh_dmg = 8;
		dh_spd = 12 + 2;
		dh_cd = 5;
		dh_mvn = 2.0 + 0.6;

		ze_hp = 80;
		ze_dmg = 8;
		ze_mvn = 1.5 + 1.0;

		he_hp = 80;
		he_dmg = 8;
		he_mvn = 1.8 + 1.0;

		be_hp = 1000;
		be_dmg = 5;
		be_mvn = 0.8 + 0.1 + 0.1 + 0.1 + 0.1 + 0.2;
		be_spd = 4000;

		boss_time = 300;

		light_points_current -= light_points_current * 0.5;
		dark_points_current -= dark_points_current * 0.5;
		morality_cap = 800;
	}
}

int main(int argc, char **argv) {

	/// Display 
	const float FPS = 60;
	int app = 1;
	int loop = 0;

	/// Screens
	int entry_screen_on = 1; // type 1 to run it all
	int menu_screen_on = 0;
	int tutorial_screen_on = 0;
	int options_screen_on = 0;
	int credits_screen_on = 0;
	int game_screen_on = 0;
	int victory_screen_on = 0;
	int defeat_screen_on = 0;

	/// Text Files Loads
	load_options();
	load_stats();
	load_spawn();
	load_level();
	load_hero();

	/// Animation
	const int maxFrame = 103;
	int currentFrame = 102;
	if (animation_on == 0) currentFrame = 102;
	if (animation_on == 1) currentFrame = 0;
	int frameCount = 0;
	int frameDelay = 4;  // animation speed
	int character_animation_slow = 25; // should be 25
	int character_levitation_slow = 10; // should be 10

	/// Setting Character State
	zombie_current_hp = ze_hp / 2;  
	zombie2_current_hp = ze_hp / 2; 
	zombie3_current_hp = ze_hp / 2; 
	zombie4_current_hp = ze_hp / 2; 
	human_current_hp = he_hp / 2;   
	human2_current_hp = he_hp / 2;  
	human3_current_hp = he_hp / 2;  
	human4_current_hp = he_hp / 2;  
	boss_current_hp = be_hp / 2;

	/// Random Generator
	time_t tt;
	source = time(&tt);
	srand(source);

	/// Structures
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;

	bool redraw = true;
	bool done = false;
	bool key[7] = { false, false, false, false, false, false, false };

	/// Initializing Allegro
	if (!al_init()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize allegro!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

	/// Installing Keyboard and Mouse
	al_install_keyboard();
	al_install_mouse();
	/*if (!al_install_keyboard) {
	al_show_native_message_box(display, "Error", "Error", "Failed to install the keyboard!",
	NULL, ALLEGRO_MESSAGEBOX_ERROR);
	return 0;
	}
	if (!al_install_mouse) {
	al_show_native_message_box(display, "Error", "Error", "Failed to install the mouse!",
	NULL, ALLEGRO_MESSAGEBOX_ERROR);
	return 0;
	}*/

	ALLEGRO_KEYBOARD_STATE keyboard;
	ALLEGRO_MOUSE_STATE state;

	/// Initializing Timer
	timer = al_create_timer(1.0 / FPS);
	if (!timer) {
		al_show_native_message_box(display, "Error", "Error", "Failed to creat the timer!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}
	float app_time = al_get_time();
	float game_time = 0;
	float menu_time = 0;

	/// Initializing Fonts
	al_init_font_addon();
	if (!al_init_font_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize font_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

	/// Initializing Fonts TTF
	al_init_ttf_addon();
	if (!al_init_ttf_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize ttf_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

		/// Fonts
		ALLEGRO_FONT *font8 = al_create_builtin_font();
		ALLEGRO_FONT *couree = al_load_ttf_font("courier.ttf", 24, 0);
		ALLEGRO_FONT *couree_8 = al_load_ttf_font("courier.ttf", 8, 1);
		ALLEGRO_FONT *couree_12 = al_load_ttf_font("courier.ttf", 12, 2);
		ALLEGRO_FONT *couree_16 = al_load_ttf_font("courier.ttf", 16, 0);
		ALLEGRO_FONT *couree_24 = al_load_ttf_font("courier.ttf", 24, 4);

	/// Initializing Images
	al_init_image_addon();
	if (!al_init_image_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize image_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

	/// Initializing Audio
	if (!al_install_audio()) {
		fprintf(stderr, "failed to initialize audio!\n");
		return -1;
	}

	/// Initializing Codec
	if (!al_init_acodec_addon()) {
		fprintf(stderr, "failed to initialize audio codecs!\n");
		return -1;
	}

	/// Initializing Sample
	if (!al_reserve_samples(5)) {
		fprintf(stderr, "failed to reserve samples!\n");
		return -1;
	}

	/// S T R U C T U R E S

		/// Entry Screen
		ALLEGRO_BITMAP *entry_screen[10]; // to musi byc i nie wiem czemu
		ALLEGRO_BITMAP *smoke = NULL;
		ALLEGRO_BITMAP *logo = NULL;
		ALLEGRO_BITMAP *screen_black = NULL;

		/// Menu Screen
		ALLEGRO_BITMAP *screen_menu_background_black = NULL;
		ALLEGRO_BITMAP *screen_menu_background_white = NULL;
		ALLEGRO_BITMAP *screen_menu_newgame_black = NULL;
		ALLEGRO_BITMAP *screen_menu_continue_black = NULL;
		ALLEGRO_BITMAP *screen_menu_tutorial_black = NULL;
		ALLEGRO_BITMAP *screen_menu_options_black = NULL;
		ALLEGRO_BITMAP *screen_menu_credits_black = NULL;
		ALLEGRO_BITMAP *screen_menu_exit_black = NULL;

		/// Tutorial Screen
		ALLEGRO_BITMAP *screen_tutorial = NULL;

		/// Options Screen
		ALLEGRO_BITMAP *screen_options = NULL;

		/// Credits Screen
		ALLEGRO_BITMAP *screen_credits = NULL;

		/// Score Screens
		ALLEGRO_BITMAP *screen_victory = NULL;
		ALLEGRO_BITMAP *screen_defeat = NULL;
		ALLEGRO_BITMAP *screen_score_continue = NULL;

		/// Buttons
		ALLEGRO_BITMAP *button_exit = NULL;
		ALLEGRO_BITMAP *button_sounds = NULL;
		ALLEGRO_BITMAP *button_music = NULL;

		/// Sky Images
		ALLEGRO_BITMAP *moon_normal = NULL;
		ALLEGRO_BITMAP *moon_blood = NULL;
		ALLEGRO_BITMAP *sky_stars_blue = NULL;
		ALLEGRO_BITMAP *sky_stars_green = NULL;
		ALLEGRO_BITMAP *sky_stars_normal = NULL;
		ALLEGRO_BITMAP *sky_stars_normal_2 = NULL;

		/// Level 1-2 Layers
		ALLEGRO_BITMAP *layer0 = NULL;
		ALLEGRO_BITMAP *layer1 = NULL;
		ALLEGRO_BITMAP *layer2 = NULL;
		ALLEGRO_BITMAP *layer3 = NULL;
		ALLEGRO_BITMAP *layer4 = NULL;
		ALLEGRO_BITMAP *layer5 = NULL;

		/// Level 5-6 Layers
		ALLEGRO_BITMAP *layer0_green = NULL;
		ALLEGRO_BITMAP *layer1_green = NULL;
		ALLEGRO_BITMAP *layer2_green = NULL;
		ALLEGRO_BITMAP *layer3_green = NULL;
		ALLEGRO_BITMAP *layer4_green = NULL;
		ALLEGRO_BITMAP *layer5_green = NULL;

		/// Level 5-6 Layers
		ALLEGRO_BITMAP *layer0_blue = NULL;
		ALLEGRO_BITMAP *layer1_blue = NULL;
		ALLEGRO_BITMAP *layer2_blue = NULL;
		ALLEGRO_BITMAP *layer3_blue = NULL;
		ALLEGRO_BITMAP *layer4_blue = NULL;
		ALLEGRO_BITMAP *layer5_blue = NULL;

		/// Level 7-8 Layers
		ALLEGRO_BITMAP *layer0_red = NULL;
		ALLEGRO_BITMAP *layer1_red = NULL;
		ALLEGRO_BITMAP *layer2_red = NULL;
		ALLEGRO_BITMAP *layer3_red = NULL;
		ALLEGRO_BITMAP *layer4_red = NULL;
		ALLEGRO_BITMAP *layer5_red = NULL;

		/// Level 9-10 Layers
		ALLEGRO_BITMAP *layer0_orange = NULL;
		ALLEGRO_BITMAP *layer1_orange = NULL;
		ALLEGRO_BITMAP *layer2_orange = NULL;
		ALLEGRO_BITMAP *layer3_orange = NULL;
		ALLEGRO_BITMAP *layer4_orange = NULL;
		ALLEGRO_BITMAP *layer5_orange = NULL;

		/// Characters Models 
		ALLEGRO_BITMAP *hero_noblood_1 = NULL;
		ALLEGRO_BITMAP *hero_noblood_2 = NULL;
		ALLEGRO_BITMAP *hero_blood_1 = NULL;
		ALLEGRO_BITMAP *hero_blood_2 = NULL;
		ALLEGRO_BITMAP *zombie_unhealed_1 = NULL;
		ALLEGRO_BITMAP *zombie_unhealed_2 = NULL;
		ALLEGRO_BITMAP *zombie_healed_1 = NULL;
		ALLEGRO_BITMAP *zombie_healed_2 = NULL;
		ALLEGRO_BITMAP *human_unhealed_1 = NULL;
		ALLEGRO_BITMAP *human_unhealed_2 = NULL;
		ALLEGRO_BITMAP *human_healed_1 = NULL;
		ALLEGRO_BITMAP *human_healed_2 = NULL;
		ALLEGRO_BITMAP *light_1 = NULL;
		ALLEGRO_BITMAP *light_2 = NULL;
		ALLEGRO_BITMAP *light_3 = NULL;
		ALLEGRO_BITMAP *light_4 = NULL;
		ALLEGRO_BITMAP *light_5 = NULL;
		ALLEGRO_BITMAP *dark_1 = NULL;
		ALLEGRO_BITMAP *dark_2 = NULL;
		ALLEGRO_BITMAP *dark_3 = NULL;
		ALLEGRO_BITMAP *dark_4 = NULL;
		ALLEGRO_BITMAP *dark_5 = NULL;
		ALLEGRO_BITMAP *boss_1 = NULL;
		ALLEGRO_BITMAP *boss_2 = NULL;

		/// Collision 
		ALLEGRO_BITMAP *zombie_collision_damaged = NULL;
		ALLEGRO_BITMAP *zombie_collision_healed = NULL;
		ALLEGRO_BITMAP *human_collision_damaged = NULL;
		ALLEGRO_BITMAP *human_collision_healed = NULL;
		ALLEGRO_BITMAP *boss_collision_damaged = NULL;
		ALLEGRO_BITMAP *boss_collision_healed = NULL;
		ALLEGRO_BITMAP *hero_collision_damaged = NULL;
		ALLEGRO_BITMAP *superhero_collision_damaged = NULL;

		/// Elements 
		ALLEGRO_BITMAP *normal_attack = NULL;
		ALLEGRO_BITMAP *normal_heal = NULL;
		ALLEGRO_BITMAP *light_attack = NULL;
		ALLEGRO_BITMAP *light_heal = NULL;
		ALLEGRO_BITMAP *light_ultimate = NULL;
		ALLEGRO_BITMAP *dark_attack = NULL;
		ALLEGRO_BITMAP *dark_heal = NULL;
		ALLEGRO_BITMAP *dark_ultimate = NULL;
		ALLEGRO_BITMAP *boss_attack = NULL;

	/// Initializing Primitives
	al_init_primitives_addon();
	if (!al_init_primitives_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize primitives_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

	/// Interface
	ALLEGRO_BITMAP *interface_background = NULL;
	ALLEGRO_BITMAP *interface_frame = NULL;
	ALLEGRO_BITMAP *interface_j_normal = NULL;
	ALLEGRO_BITMAP *interface_k_normal = NULL;
	ALLEGRO_BITMAP *interface_l_normal = NULL;
	ALLEGRO_BITMAP *interface_j_light = NULL;
	ALLEGRO_BITMAP *interface_k_light = NULL;
	ALLEGRO_BITMAP *interface_l_light = NULL;
	ALLEGRO_BITMAP *interface_j_dark = NULL;
	ALLEGRO_BITMAP *interface_k_dark = NULL;
	ALLEGRO_BITMAP *interface_l_dark = NULL;

	/// Initializing Colors
	ALLEGRO_COLOR color_white = al_map_rgb(255, 255, 255);
	ALLEGRO_COLOR color_blackk = al_map_rgb(1, 1, 1);
	ALLEGRO_COLOR color_gray = al_map_rgb(166, 166, 166);
	ALLEGRO_COLOR color_light = al_map_rgb(255, 205, 0);
	ALLEGRO_COLOR color_dark = al_map_rgb(50, 0, 200);
	ALLEGRO_COLOR color_human = al_map_rgb(8, 100, 193);
	ALLEGRO_COLOR color_zombie = al_map_rgb(0, 120, 0);
	ALLEGRO_COLOR color_red = al_map_rgb(199, 10, 10);

	/// Audio Structures
	ALLEGRO_SAMPLE *music_intro = NULL;
	ALLEGRO_SAMPLE *music_main = NULL;
	ALLEGRO_SAMPLE *sound_click = NULL;
	ALLEGRO_SAMPLE *sound_heal = NULL;
	ALLEGRO_SAMPLE *sound_heal2 = NULL;
	ALLEGRO_SAMPLE *sound_hero_hit = NULL;
	ALLEGRO_SAMPLE *sound_human_hit = NULL;
	ALLEGRO_SAMPLE *sound_zombie_hit = NULL;
	ALLEGRO_SAMPLE *sound_shoot = NULL;
	ALLEGRO_SAMPLE *sound_light_j = NULL;
	ALLEGRO_SAMPLE *sound_light_k = NULL;
	ALLEGRO_SAMPLE *sound_ultimate = NULL;
	ALLEGRO_SAMPLE *sound_step = NULL;

	ALLEGRO_SAMPLE_ID *id_1 = 1;
	ALLEGRO_SAMPLE_ID *id_2 = 2;
	ALLEGRO_SAMPLE_ID *id_3 = 3;
	ALLEGRO_SAMPLE_ID *id_4 = 4;
	
	/// Initializing Display
	display = al_create_display(SCREEN_W, SCREEN_H);
	al_set_window_title(display, "20M812-V1.7 by Adam Drabik");
	al_set_new_display_flags(ALLEGRO_WINDOWED);
	if (!display) {
		al_show_native_message_box(display, "Error", "Error", "Failed to create the display!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

	/// L O A D S 
	{
		/// Smoke
		smoke = al_load_bitmap("smoke.png");
		logo = al_load_bitmap("logo.png");
		screen_black = al_load_bitmap("screen_black.png");

		/// Menu Screen Images Load
		screen_menu_background_black = al_load_bitmap("screen_menu_background_black.png");
		screen_menu_background_white = al_load_bitmap("screen_menu_background_white.png");
		screen_menu_newgame_black = al_load_bitmap("screen_menu_newgame_black.png");
		screen_menu_continue_black = al_load_bitmap("screen_menu_continue_black.png");
		screen_menu_tutorial_black = al_load_bitmap("screen_menu_tutorial_black.png");
		screen_menu_options_black = al_load_bitmap("screen_menu_options_black.png");
		screen_menu_credits_black = al_load_bitmap("screen_menu_credits_black.png");
		screen_menu_exit_black = al_load_bitmap("screen_menu_exit_black.png");

		/// Screens Images Load
		screen_tutorial = al_load_bitmap("screen_tutorial.png");
		screen_options = al_load_bitmap("screen_options.png");
		screen_credits = al_load_bitmap("screen_credits.png");
		screen_victory = al_load_bitmap("screen_victory.png");
		screen_defeat = al_load_bitmap("screen_defeat.png");
		screen_score_continue = al_load_bitmap("screen_score_continue.png");

		/// Buttons Images Load
		button_exit = al_load_bitmap("buttonx.png");
		button_sounds = al_load_bitmap("buttonsounds.png");
		button_music = al_load_bitmap("buttonmusic.png");

		/// Terrain Images Load
		moon_normal = al_load_bitmap("moon_normal.png");
		moon_blood = al_load_bitmap("moon_blood.png");
		sky_stars_blue = al_load_bitmap("sky_stars_blue.png");
		sky_stars_green = al_load_bitmap("sky_stars_green.png");
		sky_stars_normal = al_load_bitmap("sky_stars_normal.png");
		sky_stars_normal_2 = al_load_bitmap("sky_stars_normal_2.png");

		/// Level 1-2 Layers Images Load
		layer0 = al_load_bitmap("layer0.png");
		layer1 = al_load_bitmap("layer1.png");
		layer2 = al_load_bitmap("layer2.png");
		layer3 = al_load_bitmap("layer3.png");
		layer4 = al_load_bitmap("layer4.png");
		layer5 = al_load_bitmap("layer5.png");

		/// Level 3-4 Layers Images Load
		layer0_green = al_load_bitmap("layer0_green.png");
		layer1_green = al_load_bitmap("layer1_green.png");
		layer2_green = al_load_bitmap("layer2_green.png");
		layer3_green = al_load_bitmap("layer3_green.png");
		layer4_green = al_load_bitmap("layer4_green.png");
		layer5_green = al_load_bitmap("layer5_green.png");

		/// Level 5-6 Layers Images Load
		layer0_blue = al_load_bitmap("layer0_blue.png");
		layer1_blue = al_load_bitmap("layer1_blue.png");
		layer2_blue = al_load_bitmap("layer2_blue.png");
		layer3_blue = al_load_bitmap("layer3_blue.png");
		layer4_blue = al_load_bitmap("layer4_blue.png");
		layer5_blue = al_load_bitmap("layer5_blue.png");

		/// Level 7-8 Layers Images Load
		layer0_red = al_load_bitmap("layer0_red.png");
		layer1_red = al_load_bitmap("layer1_red.png");
		layer2_red = al_load_bitmap("layer2_red.png");
		layer3_red = al_load_bitmap("layer3_red.png");
		layer4_red = al_load_bitmap("layer4_red.png");
		layer5_red = al_load_bitmap("layer5_red.png");

		/// Level 9-10 Layers Images Load
		layer0_orange = al_load_bitmap("layer0_orange.png");
		layer1_orange = al_load_bitmap("layer1_orange.png");
		layer2_orange = al_load_bitmap("layer2_orange.png");
		layer3_orange = al_load_bitmap("layer3_orange.png");
		layer4_orange = al_load_bitmap("layer4_orange.png");
		layer5_orange = al_load_bitmap("layer5_orange.png");

		/// Character Models Images Load
		hero_noblood_1 = al_load_bitmap("hero_noblood_1.png");
		hero_noblood_2 = al_load_bitmap("hero_noblood_2.png");
		hero_blood_1 = al_load_bitmap("hero_blood_1.png");
		hero_blood_2 = al_load_bitmap("hero_blood_2.png");
		zombie_unhealed_1 = al_load_bitmap("zombie_unhealed_1.png");
		zombie_unhealed_2 = al_load_bitmap("zombie_unhealed_2.png");
		zombie_healed_1 = al_load_bitmap("zombie_healed_1.png");
		zombie_healed_2 = al_load_bitmap("zombie_healed_2.png");
		human_unhealed_1 = al_load_bitmap("human_unhealed_1.png");
		human_unhealed_2 = al_load_bitmap("human_unhealed_2.png");
		human_healed_1 = al_load_bitmap("human_healed_1.png");
		human_healed_2 = al_load_bitmap("human_healed_2.png");
		light_1 = al_load_bitmap("light_1.png");
		light_2 = al_load_bitmap("light_2.png");
		light_3 = al_load_bitmap("light_3.png");
		light_4 = al_load_bitmap("light_4.png");
		light_5 = al_load_bitmap("light_5.png");
		dark_1 = al_load_bitmap("dark_1.png");
		dark_2 = al_load_bitmap("dark_2.png");
		dark_3 = al_load_bitmap("dark_3.png");
		dark_4 = al_load_bitmap("dark_4.png");
		dark_5 = al_load_bitmap("dark_5.png");
		boss_1 = al_load_bitmap("boss_1.png");
		boss_2 = al_load_bitmap("boss_2.png");

		/// Collision
		zombie_collision_damaged = al_load_bitmap("zombie_collision_damaged.png");
		zombie_collision_healed = al_load_bitmap("zombie_collision_healed.png");
		human_collision_damaged = al_load_bitmap("human_collision_damaged.png");
		human_collision_healed = al_load_bitmap("human_collision_healed.png");
		boss_collision_damaged = al_load_bitmap("boss_collision_damaged.png");
		boss_collision_healed = al_load_bitmap("boss_collision_healed.png");
		hero_collision_damaged = al_load_bitmap("hero_collision_damaged.png");
		superhero_collision_damaged = al_load_bitmap("superhero_collision_damaged.png");

		/// Elements Images Load
		normal_attack = al_load_bitmap("normal_attack.png");
		normal_heal = al_load_bitmap("normal_heal.png");
		light_attack = al_load_bitmap("light_attack.png");
		light_heal = al_load_bitmap("light_heal.png");
		light_ultimate = al_load_bitmap("light_ultimate.png");
		dark_attack = al_load_bitmap("dark_attack.png");
		dark_heal = al_load_bitmap("dark_heal.png");
		dark_ultimate = al_load_bitmap("dark_ultimate.png");
		boss_attack = al_load_bitmap("boss_attack.png");

		/// Interface
		interface_background = al_load_bitmap("interface_background.png");
		interface_frame = al_load_bitmap("interface_frame.png");
		interface_j_normal = al_load_bitmap("interface_j_normal.png");
		interface_k_normal = al_load_bitmap("interface_k_normal.png");
		interface_l_normal = al_load_bitmap("interface_l_normal.png");
		interface_j_light = al_load_bitmap("interface_j_light.png");
		interface_k_light = al_load_bitmap("interface_k_light.png");
		interface_l_light = al_load_bitmap("interface_l_light.png");
		interface_j_dark = al_load_bitmap("interface_j_dark.png");
		interface_k_dark = al_load_bitmap("interface_k_dark.png");
		interface_l_dark = al_load_bitmap("interface_l_dark.png");

		/// Audio
		music_intro = al_load_sample("music_intro.wav");
		music_main = al_load_sample("music_main.ogg");
		sound_click = al_load_sample("sound_click.wav");
		sound_heal = al_load_sample("sound_heal.wav");
		sound_heal2 = al_load_sample("sound_heal2.wav");
		sound_hero_hit = al_load_sample("sound_hero_hit.wav");
		sound_human_hit = al_load_sample("sound_human_hit.wav");
		sound_zombie_hit = al_load_sample("sound_zombie_hit.wav");
		sound_shoot = al_load_sample("sound_shoot.wav");
		sound_light_j = al_load_sample("sound_light_j.wav");
		sound_light_k = al_load_sample("sound_light_k.wav");
		sound_ultimate = al_load_sample("sound_ultimate.wav");
		sound_step = al_load_sample("sound_step.wav");
	}

	if (music_on == 1) al_play_sample(music_intro, music_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);

	/// Initializing Events
	event_queue = al_create_event_queue();
	if (!event_queue) {
		al_show_native_message_box(display, "Error", "Error", "Failed to create event_queue!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

	al_register_event_source(event_queue, al_get_display_event_source(display));

	al_register_event_source(event_queue, al_get_timer_event_source(timer));

	al_register_event_source(event_queue, al_get_mouse_event_source());

	al_register_event_source(event_queue, al_get_keyboard_event_source());

	al_start_timer(timer);

	/// Concole notifications
	{
		printf("\n 20M812-V%i.%i.%i\n\n", app_version, app_build, app_update);
		printf("Date: %i.%i.%i\n\n", date_day, date_month, date_year);
		printf("Level: %i\n\n", game_level);
		printf("Options:\n music on = %i (%.2f)\n sounds on = %i (%.2f)\n animation on = %i\n curosr on = %i\n game hud on = %i\n app hud on = %i\n enemy hud on = %i\n interface on = %i\n\n", music_on, music_volume, sounds_on, sound_volume, animation_on, enemy_hud_on, stats_hud_on, app_hud_on, game_hud_on, interface_on);
		printf("Spawn:\n Spawn Zombie = %i\n Spawn Human = %i\n Spawn Boss = %i\n\n", zombie_density, human_density, boss_density);
		printf("Hero:\n Health Points = %0.f\n Light Points = %0.f/%0.f\n Dark Points = %0.f/%0.f\n\n", hero_current_hp, light_points_current, light_points_max, dark_points_current, dark_points_max);
		//printf("Stats:\n normal hero health = %.1f\n normal hero damage = %.1f\n normal hero speed = %.1f\n normal hero cooldown = %.1f\n normal hero movement = %.1f\n light hero health = %.1f\n light hero damage = %.1f\n light hero speed = %.1f\n light hero cooldown = %.1f\n light hero movement = %.1f\n dark hero health = %.1f\n dark hero damage = %.1f\n dark hero speed = %.1f\n dark hero cooldown = %.1f\n normal dark movement = %.1f\n zombie enemy health = %.1f\n zombie enemy damage = %.1f\n zombie enemy movement = %.1f\n human enemy health = %.1f\n human enemy damage = %.1f\n human enemy movement = %.1f\n boss health = %.1f\n boss damage = %.1f\n boss movement = %.1f\n\n", nh_hp, nh_dmg, nh_spd, morality, nh_mvn, lh_hp, lh_dmg, lh_spd, lh_cd, lh_mvn, dh_hp, dh_dmg, dh_spd, dh_cd, dh_mvn, ze_hp, ze_dmg, ze_mvn, he_hp, he_dmg, he_mvn, be_hp, be_dmg, be_mvn);
	}

	///  G A M E  L O O P
	while (loop == 0)
	{
		ALLEGRO_EVENT ev;
		//ALLEGRO_TIMEOUT timeout;
		al_wait_for_event(event_queue, &ev);

		extern volatile int freeze_mouse_flag(state); 
	

		/// Windows Screen Exit
		if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			break;
		}

		/// Frame Counting
		if (ev.type == ALLEGRO_EVENT_TIMER)
		{
			if (++frameCount >= frameDelay && entry_screen_on == 1)
			{
				if (++currentFrame >= maxFrame)
					currentFrame = 0;
				frameCount = 0;
			}
		}

		/// Reading Keyboard and Mouse
		al_get_keyboard_state(&keyboard);
		al_get_mouse_state(&state);
		al_get_mouse_event_source();

		/// Menu Background
		if (game_screen_on == 0)
		{
			al_clear_to_color(al_map_rgb(0, 0, 0));
		}

		/// Entry Screen Animation
		if (entry_screen_on == 1)
		{
			if (animation_on == 1)
			{
				if (currentFrame <= 78)	al_draw_bitmap(logo, 0, 0, 0);
				if (currentFrame >= 80) al_draw_bitmap(screen_menu_background_black, 0, 0, 0);

				if (currentFrame >= 58 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 59 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 60 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 61 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 62 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 63 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 64 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 65 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 66 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 67 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 68 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 69 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 70 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 71 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 72 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 73 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 74 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 75 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 76 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 77 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 78 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 79 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 80 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 81 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 82 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 83 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);
				if (currentFrame >= 84 - 5 && currentFrame <= 80) al_draw_bitmap(screen_black, 0, 0, 0);


				if (currentFrame == 80 || currentFrame == 79 || currentFrame == 78)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 81)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 82)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 83)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 84)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 85)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 86)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 87)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 88)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 89)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 90)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
				}

				if (currentFrame == 91)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);

				}

				if (currentFrame == 92)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);

				}

				if (currentFrame == 93)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);

				}

				if (currentFrame == 94)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);

				}

				if (currentFrame == 95)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);

				}

				if (currentFrame == 96)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);

				}

				if (currentFrame == 97)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);

				}

				if (currentFrame == 98)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);
					al_draw_bitmap(screen_black, 0, 0, 0);

				}

				if (currentFrame == 99)
				{
					al_draw_bitmap(screen_black, 0, 0, 0);

				}

				if (currentFrame < 120 && currentFrame > 80)
				{
					al_draw_bitmap(smoke, 0, 0, 0);
					al_draw_bitmap(smoke, 0, 0, 1);
				}

				if (currentFrame == 101)
				{
					al_clear_to_color(color_white);
					al_draw_bitmap(screen_menu_background_white, 0, 0, 0);
				}

			}
		}

		/// Entry Screen Off
		if (currentFrame == 102)
		{
			entry_screen_on = 0;
			menu_screen_on = 1;
		}

		/// Menu Screen On
		if (menu_screen_on == 1)
		{
		
			al_draw_bitmap(screen_menu_background_black, 0, 0, 0);

			if (state.x <= 837 && state.x >= 442 && state.y <= 219 + 77 && state.y >= 219)
			{
				al_draw_bitmap(screen_menu_newgame_black, 0, 0, 0);
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
					if (game_screen_on == 0 && tutorial_screen_on == 0 && credits_screen_on == 0 && options_screen_on == 0)
					{
						al_stop_samples();
						if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						if (music_on == 2) music_on = 1;
						if (music_on == 1) al_play_sample(music_main, music_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
						/// Game Setting
						game_screen_on = 1;
						game_level = 1;
						frameCount = 0;
						reset_health_points();
						reset_enemy_position();
						reset_stats();

					}
			}

			if (state.x <= 837 && state.x >= 442 && state.y <= 319 + 77 && state.y >= 319)
			{
				al_draw_bitmap(screen_menu_continue_black, 0, 0, 0);
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
					if (game_screen_on == 0 && tutorial_screen_on == 0 && credits_screen_on == 0 && options_screen_on == 0)
					{
						al_stop_samples();
						if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						if (music_on == 2) music_on = 1;
						if (music_on == 1) al_play_sample(music_main, music_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
						game_screen_on = 1;
						frameCount = 0;
						set_stats();
						reset_health_points();
						reset_enemy_position();
						boss_alive = -1;
					}
			}

			if (state.x <= 837 && state.x >= 442 && state.y <= 419 + 77 && state.y >= 419)
			{
				al_draw_bitmap(screen_menu_tutorial_black, 0, 0, 0);
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
					if (game_screen_on == 0 && tutorial_screen_on == 0 && credits_screen_on == 0 && options_screen_on == 0)
					{
						if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						tutorial_screen_on = 1;
					}
			}

			if (state.x <= 837 && state.x >= 442 && state.y <= 519 + 77 && state.y >= 519)
			{
				al_draw_bitmap(screen_menu_options_black, 0, 0, 0);
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
					if (game_screen_on == 0 && tutorial_screen_on == 0 && credits_screen_on == 0 && options_screen_on == 0)
					{
						if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						options_screen_on = 1;
					}
			}

			if (state.x <= 837 && state.x >= 442 && state.y <= 619 + 77 && state.y >= 619)
			{
				al_draw_bitmap(screen_menu_credits_black, 0, 0, 0);
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
					if (game_screen_on == 0 && tutorial_screen_on == 0 && credits_screen_on == 0 && options_screen_on == 0)
					{
						if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						credits_screen_on = 1;
					}
			}

			if (state.x <= 837 && state.x >= 442 && state.y <= 719 + 77 && state.y >= 719)
			{
				al_draw_bitmap(screen_menu_exit_black, 0, 0, 0);
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
					if (game_screen_on == 0 && tutorial_screen_on == 0 && credits_screen_on == 0 && options_screen_on == 0)
					{
						if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						loop = 1;
					}
			}

			if (state.x <= 681 && state.x >= 675 && state.y <= 109 && state.y >= 103)
			{
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
					if (game_screen_on == 0 && tutorial_screen_on == 0 && credits_screen_on == 0 && options_screen_on == 0)
					{
						//if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						game_level += 1;																																					// szzzzzzzzzz... to nasza tajemnica !!!
						if (game_level > 10) game_level = 1;
					}
			}

		}

		/// Game Screen On
		if (game_screen_on == 1)
		{

			al_set_mouse_xy(display, -2000, -2000); // EUREKAAAAAAAAAAAAAAAAAA ! :)

			/// Setting Current HP after downgrade hero class
			if (percent_hp >= 100 && light_points_current < light_points_max && dark_points_current < dark_points_max) hero_current_hp = nh_hp;
			if (percent_hp >= 100 && light_points_current >= light_points_max) hero_current_hp = lh_hp;
			if (percent_hp >= 100 && dark_points_current >= dark_points_max) hero_current_hp = dh_hp;

			/// Time Cound
			game_time = 1.66666666666*frameCount / 100; // frameCount -> sekundy
			
			/// Turns of Screens
			menu_screen_on = 0;
			victory_screen_on = 0;
			defeat_screen_on = 0;

			/// T E R R A I N 

			if (game_level == 1)
			{
				/// Terrain Draw
				al_draw_bitmap(layer0_blue, 0, 0, 0); //niebo
				al_draw_bitmap(sky_stars_normal, 0 - 2 * game_time / 2, -60, 0); //gwiazdy
				al_draw_bitmap(moon_normal, 1300 - 15 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1, 0, 0, 0); //cien gor
				al_draw_bitmap(layer2, 1300 - 15 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3, 0, 0, 0); //gory i pole
				al_draw_bitmap(layer4, 0, 0, 0); //las prawy 
				//al_draw_bitmap(layer5, 0, 0, 0); //las lewy

			}

			if (game_level == 2)
			{
				al_draw_bitmap(layer0_blue, 0, 0, 0); //niebo
				al_draw_bitmap(sky_stars_normal_2, 0 - 2 * game_time / 2, -60, 0); //gwiazdy
				al_draw_bitmap(moon_normal, 1300 - 15 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1, 0, 0, 0); //cien gor
				al_draw_bitmap(layer2, 1300 - 15 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3, 0, 0, 0); //gory i pole
												 //al_draw_bitmap(layer4, 0, 0, 0); //las prawy 
				al_draw_bitmap(layer5, 0, 0, 0); //las lewy
			}

			if (game_level == 3)
			{
				al_draw_bitmap(layer0_green, 0, 0, 0); //niebo
				al_draw_bitmap(sky_stars_green, 0 - 2 * game_time / 2, -60, 0);
				al_draw_bitmap(moon_normal, 1300 - 10 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1_green, 0, 0, 0); //cien gor
				al_draw_bitmap(layer2_green, 1300 - 10 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3_green, 0, 0, 0); //gory i pole
				al_draw_bitmap(layer4_green, 0, 0, 0); //las prawy 
													   //al_draw_bitmap(layer5_green, 0, 0, 0); //las lewy
			}

			if (game_level == 4)
			{
				al_draw_bitmap(layer0_green, 0, 0, 0); //niebo
				al_draw_bitmap(sky_stars_green, 0 - 2 * game_time / 2, -60, 0);
				al_draw_bitmap(moon_normal, 1300 - 10 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1_green, 0, 0, 0); //cien gor
				al_draw_bitmap(layer2_green, 1300 - 10 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3_green, 0, 0, 0); //gory i pole
				al_draw_bitmap(layer4_green, 0, 0, 0); //las prawy 
													   //al_draw_bitmap(layer5_green, 0, 0, 0); //las lewy
			}

			if (game_level == 5)
			{
				al_draw_bitmap(layer0_blue, 0, 0, 0); //niebo
				al_draw_bitmap(sky_stars_blue, 0 - 2 * game_time / 2, -60, 0);
				al_draw_bitmap(sky_stars_blue, 200 - game_time / 2, -60, 0);
				al_draw_bitmap(moon_normal, 1300 - 10 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1_blue, 0, 0, 0); //cien gor
				al_draw_bitmap(layer2_blue, 1300 - 10 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3_blue, 0, 0, 0); //gory i pole
				al_draw_bitmap(layer4_blue, 0, 0, 0); //las prawy 
				al_draw_bitmap(layer5_blue, 0, 0, 0); //las lewy
			}

			if (game_level == 6)
			{
				al_draw_bitmap(layer0_blue, 0, 0, 0); //niebo
				al_draw_bitmap(sky_stars_blue, 0 - 3 * game_time, -60, 0);
				al_draw_bitmap(sky_stars_blue, 200 - 2 * game_time, -60, 0);
				al_draw_bitmap(moon_normal, 1300 - 10 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1_blue, 0, 0, 0); //cien gor
				al_draw_bitmap(layer2_blue, 1300 - 10 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3_blue, 0, 0, 0); //gory i pole
				al_draw_bitmap(layer4_blue, 0, 0, 0); //las prawy 
				al_draw_bitmap(layer5_blue, 0, 0, 0); //las lewy
			}

			if (game_level == 7)
			{
				al_draw_bitmap(layer0_red, 0, 0, 0); //niebo
				al_draw_bitmap(moon_blood, 1300 - 15 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1_red, 0, 0, 0); //cien gor
				al_draw_bitmap(layer2_red, 1300 - 15 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3_red, 0, 0, 0); //gory i pole
													 //al_draw_bitmap(layer4_red, 0, 0, 0); //las prawy 
				al_draw_bitmap(layer5_red, 0, 0, 0); //las lewy
			}

			if (game_level == 8)
			{
				al_draw_bitmap(layer0_red, 0, 0, 0); //niebo
				al_draw_bitmap(moon_blood, 1300 - 15 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1_red, 0, 0, 0); //cien gor
				al_draw_bitmap(layer2_red, 1300 - 15 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3_red, 0, 0, 0); //gory i pole
													 //al_draw_bitmap(layer4_red, 0, 0, 0); //las prawy 
													 //al_draw_bitmap(layer5_red, 0, 0, 0); //las lewy
			}

			if (game_level == 9)
			{
				al_draw_bitmap(layer0_orange, 0, 0, 0); //niebo
				al_draw_bitmap(moon_blood, 1300 - 8 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1_orange, 0, 0, 0); //cien gor
														//al_draw_bitmap(layer2_orange, 1300 - 10 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3_orange, 0, 0, 0); //gory i pole
														//al_draw_bitmap(layer4_orange, 0, 0, 0); //las prawy 
														//al_draw_bitmap(layer5_orange, 0, 0, 0); //las lewy
			}

			if (game_level == 10)
			{
				al_draw_bitmap(layer0_orange, 0, 0, 0); //niebo
														al_draw_bitmap(moon_blood, 1300 - 8 * game_time, 65, 0); //ksiezyc
				al_draw_bitmap(layer1_orange, 0, 0, 0); //cien gor
														//al_draw_bitmap(layer2_orange, 1300 - 10 * game_time - (SCREEN_W / 2) + 143, 65 - (SCREEN_H / 2) + 20, 0); //blask ksiezyca
				al_draw_bitmap(layer3_orange, 0, 0, 0); //gory i pole
														//al_draw_bitmap(layer4_orange, 0, 0, 0); //las prawy 
														//al_draw_bitmap(layer5_orange, 0, 0, 0); //las lewy
			}

			/// G A M E  M E C H A N I C S

			/// Elements Movement
			if (shoot_arrow == 1)
			{
				arrow();
				if (light_points_current < light_points_max && dark_points_current < dark_points_max) al_draw_bitmap(normal_attack, arrow_x, arrow_y + 67, 0);
				if (light_points_current >= light_points_max) al_draw_bitmap(light_attack, arrow_x + 70 + 35, arrow_y + 34, 0);
				if (dark_points_current >= dark_points_max) al_draw_bitmap(dark_attack, arrow_x + 70 + 35, arrow_y + 34, 0);
			}

			if (shoot_heal == 1)
			{
				heal();
				if (light_points_current < light_points_max && dark_points_current < dark_points_max) al_draw_bitmap(normal_heal, heal_x, heal_y + 66, 0);
				if (light_points_current >= light_points_max) al_draw_bitmap(light_heal, heal_x + 70 + 33, heal_y + 55, 0);
				if (dark_points_current >= dark_points_max) al_draw_bitmap(dark_heal, heal_x + 70 + 33, heal_y + 55, 0);
			}

			if (shoot_ultimate == 1)
			{
				ultimate();
				if (light_points_current >= light_points_max) al_draw_bitmap(light_ultimate, hero_x + 70 + 40, hero_y + 48, 0);
				if (dark_points_current >= dark_points_max) al_draw_bitmap(dark_ultimate, hero_x + 70 + 40, hero_y + 48, 0);
			}
			
			/// Fireball Movement
			fireball_spawn();

			if (fireball_alive == 1 && boss_density >= 1 && boss_alive == 1 && boss_current_hp > 0)
			{
				al_draw_bitmap(boss_attack, fireball_x, fireball_y, 0);
			}

			if (fireball_alive == 0 && boss_density >= 1)
			{
				fireball_x = boss_x + 200; // +200
				fireball_y = boss_y + 100;  // +100
				fireball_alive = 1;
			}

			/// Zombie Spawn
			zombie_spawn();

			/// Zombie 1 Movement and Animation
			if (zombie_alive == 1 && zombie_density >= 1)
			{
				if (zombie_current_hp < ze_hp && zombie_current_hp > 0)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(zombie_unhealed_1, zombie_x, zombie_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(zombie_unhealed_2, zombie_x, zombie_y, 0);
				}

				if (zombie_current_hp >= ze_hp) 
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(zombie_healed_1, zombie_x + 30, zombie_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(zombie_healed_2, zombie_x + 30, zombie_y, 0);
				}

				if (zombie_current_hp == 0) 
				{
					al_draw_bitmap(zombie_collision_damaged, zombie_x, zombie_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie_x, zombie_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie_x, zombie_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie_x, zombie_y, 0);
					zombie_current_hp -= 1;
				}

			}

			if (zombie_alive == 0 && zombie_density >= 1)
			{
				random_z1x = rand() % (1);
				random_z1y = rand() % (1280 - 288 - 151 - 251 - 4);
				zombie_x = random_z1x + 1300;
				zombie_y = random_z1y + 288;
				zombie_alive = 1;
				
			}

			/// Zombie 2 Movement and Animation
			if (zombie2_alive == 1 && zombie_density >= 2)
			{
				if (zombie2_current_hp < ze_hp && zombie2_current_hp > 0)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(zombie_unhealed_1, zombie2_x, zombie2_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(zombie_unhealed_2, zombie2_x, zombie2_y, 0);
				}

				if (zombie2_current_hp >= ze_hp)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(zombie_healed_1, zombie2_x + 30, zombie2_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(zombie_healed_2, zombie2_x + 30, zombie2_y, 0);
				}

				if (zombie2_current_hp == 0) 
				{
					al_draw_bitmap(zombie_collision_damaged, zombie2_x, zombie2_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie2_x, zombie2_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie2_x, zombie2_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie2_x, zombie2_y, 0);
					zombie2_current_hp -= 1;
				}

			}

			if (zombie2_alive == 0 && zombie_density >= 2)
			{
				random_z2x = rand() % (430);
				random_z2y = rand() % (1280 - 288 - 151 - 251 - 4);
				zombie2_x = random_z2x + 1300 + 320;
				zombie2_y = random_z2y + 288;
				zombie2_alive = 1;
				
			}

			/// Zombie 3 Movement and Animation
			if (zombie3_alive == 1 && zombie_density >= 3)
			{
				if (zombie3_current_hp < ze_hp && zombie3_current_hp > 0)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(zombie_unhealed_1, zombie3_x, zombie3_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(zombie_unhealed_2, zombie3_x, zombie3_y, 0);
				}

				if (zombie3_current_hp >= ze_hp)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(zombie_healed_1, zombie3_x + 30, zombie3_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(zombie_healed_2, zombie3_x + 30, zombie3_y, 0);
				}

				if (zombie3_current_hp == 0)
				{
					al_draw_bitmap(zombie_collision_damaged, zombie3_x, zombie3_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie3_x, zombie3_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie3_x, zombie3_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie3_x, zombie3_y, 0);
					zombie3_current_hp -= 1;
				}
			}

			if (zombie3_alive == 0 && zombie_density >= 3)
			{
				random_z3x = rand() % (430 + 430);
				random_z3y = rand() % (1280 - 288 - 151 - 251 - 4);
				zombie3_x = random_z3x + 1300 + 320 + 320;
				zombie3_y = random_z3y + 288;
				zombie3_alive = 1;
				
			}

			/// Zombie 4 Movement and Animation
			if (zombie4_alive == 1 && zombie_density >= 4)
			{
				if (zombie4_current_hp < ze_hp && zombie4_current_hp > 0)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(zombie_unhealed_1, zombie4_x, zombie4_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(zombie_unhealed_2, zombie4_x, zombie4_y, 0);
				}

				if (zombie4_current_hp >= ze_hp)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(zombie_healed_1, zombie4_x + 30, zombie4_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(zombie_healed_2, zombie4_x + 30, zombie4_y, 0);
				}

				if (zombie4_current_hp == 0)
				{
					al_draw_bitmap(zombie_collision_damaged, zombie4_x, zombie4_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie4_x, zombie4_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie4_x, zombie4_y, 0);
					al_draw_bitmap(zombie_collision_damaged, zombie4_x, zombie4_y, 0);
					zombie4_current_hp -= 1;
				}
			}

			if (zombie4_alive == 0 && zombie_density >= 4)
			{
				random_z4x = rand() % (430 + 430 + 430);
				random_z4y = rand() % (1280 - 288 - 151 - 251 - 4);
				zombie4_x = random_z4x + 1300 + 320 + 320 + 320;
				zombie4_y = random_z4y + 288;
				zombie4_alive = 1;

			}

			/// Human Spawn
			human_spawn();

			/// Human 1 Movement and Animation
			if (human_alive == 1 && human_density >= 1)
			{
				if (human_current_hp < ze_hp && human_current_hp > 0)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(human_unhealed_1, human_x, human_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(human_unhealed_2, human_x, human_y, 0);
				}

				if (human_current_hp >= ze_hp)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(human_healed_1, human_x, human_y + 13, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(human_healed_2, human_x, human_y + 13, 0);
				}

				if (human_current_hp == 0)
				{
					al_draw_bitmap(human_collision_damaged, human_x, human_y, 0);
					al_draw_bitmap(human_collision_damaged, human_x, human_y, 0);
					al_draw_bitmap(human_collision_damaged, human_x, human_y, 0);
					al_draw_bitmap(human_collision_damaged, human_x, human_y, 0);
					human_current_hp -= 1;
				}
			}

			if (human_alive == 0 && human_density >= 1)
			{
				random_h1x = rand() % (1);
				random_h1y = rand() % (1280 - 288 - 151 - 251 - 4);
				human_x = random_h1x + 1300;
				human_y = random_h1y + 288;
				human_alive = 1;
				
			}

			/// Human 2 Movement and Animation
			if (human2_alive == 1 && human_density >= 2)
			{
				if (human2_current_hp < ze_hp && human2_current_hp > 0)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(human_unhealed_1, human2_x, human2_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(human_unhealed_2, human2_x, human2_y, 0);
				}

				if (human2_current_hp >= ze_hp)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(human_healed_1, human2_x, human2_y + 13, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(human_healed_2, human2_x, human2_y + 13, 0);
				}

				if (human2_current_hp == 0)
				{
					al_draw_bitmap(human_collision_damaged, human2_x, human2_y, 0);
					al_draw_bitmap(human_collision_damaged, human2_x, human2_y, 0);
					al_draw_bitmap(human_collision_damaged, human2_x, human2_y, 0);
					al_draw_bitmap(human_collision_damaged, human2_x, human2_y, 0);
					human2_current_hp -= 1;
				}
			}

			if (human2_alive == 0 && human_density >= 2)
			{
				random_h2x = rand() % (430);
				random_h2y = rand() % (1280 - 288 - 151 - 251 - 4);
				human2_x = random_h2x + 1300 + 320;
				human2_y = random_h2y + 288;
				human2_alive = 1;
				
			}

			/// Human 3 Movement and Animation
			if (human3_alive == 1 && human_density >= 3)
			{
				if (human3_current_hp < ze_hp && human3_current_hp > 0)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(human_unhealed_1, human3_x, human3_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(human_unhealed_2, human3_x, human3_y, 0);
				}

				if (human3_current_hp >= ze_hp)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(human_healed_1, human3_x, human3_y + 13, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(human_healed_2, human3_x, human3_y + 13, 0);
				}

				if (human3_current_hp == 0)
				{
					al_draw_bitmap(human_collision_damaged, human3_x, human3_y, 0);
					al_draw_bitmap(human_collision_damaged, human3_x, human3_y, 0);
					al_draw_bitmap(human_collision_damaged, human3_x, human3_y, 0);
					al_draw_bitmap(human_collision_damaged, human3_x, human3_y, 0);
					human3_current_hp -= 1;
				}
			}

			if (human3_alive == 0 && human_density >= 3)
			{
				random_h3x = rand() % (430 + 430);
				random_h3y = rand() % (1280 - 288 - 151 - 251 - 4);
				human3_x = random_h3x + 1300 + 320 + 320;
				human3_y = random_h3y + 288;
				human3_alive = 1;
				
			}

			/// Human 4 Movement and Animation
			if (human4_alive == 1 && human_density >= 4)
			{
				if (human4_current_hp < ze_hp && human4_current_hp > 0)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(human_unhealed_1, human4_x, human4_y, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(human_unhealed_2, human4_x, human4_y, 0);
				}

				if (human4_current_hp >= ze_hp)
				{
					if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(human_healed_1, human4_x, human4_y + 13, 0);
					if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(human_healed_2, human4_x, human4_y + 13, 0);
				}

				if (human4_current_hp == 0)
				{
					al_draw_bitmap(human_collision_damaged, human4_x, human4_y, 0);
					al_draw_bitmap(human_collision_damaged, human4_x, human4_y, 0);
					al_draw_bitmap(human_collision_damaged, human4_x, human4_y, 0);
					al_draw_bitmap(human_collision_damaged, human4_x, human4_y, 0);
					human4_current_hp -= 1;
				}

			}

			if (human4_alive == 0 && human_density >= 4)
			{
				random_h4x = rand() % (430 + 430);
				random_h4y = rand() % (1280 - 288 - 151 - 251 - 4);
				human4_x = random_h4x + 1300 + 320 + 320;
				human4_y = random_h4y + 288;
				human4_alive = 1;

			}

			boss_spawn();

			/// Boss Movement 
			if (boss_alive == 1 && boss_density >= 1)
			{
				if (boss_current_hp < be_hp && boss_current_hp > 0)
				{
					if (frameCount / 30 % 2 == 0) al_draw_bitmap(boss_1, boss_x, boss_y, 0);
					if (frameCount / 30 % 2 == 1) al_draw_bitmap(boss_2, boss_x, boss_y, 0);
				}

			}

			if (boss_alive == 0 && boss_density >= 1)
			{
				boss_x = 1300;
				boss_y = 50; // bylo 100
				boss_alive = 1;

			}

			/// Hero Movement
			if (ev.type == ALLEGRO_EVENT_TIMER)
			{
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (key[KEY_W] && hero_y >= nh_mvn && hero_y >= 290)
					{
						//if (sounds_on == 1) al_play_sample(sound_step, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						hero_y -= nh_mvn;
					}

					if (key[KEY_S] && hero_y <= SCREEN_H - 157 - nh_mvn)
					{
						//if (sounds_on == 1) al_play_sample(sound_step, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						hero_y += nh_mvn;
					}

					if (key[KEY_A] && hero_x >= nh_mvn)
					{
						//if (sounds_on == 1) al_play_sample(sound_step, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						hero_x -= nh_mvn;
					}

					if (key[KEY_D] && hero_x <= SCREEN_W - 77 - nh_mvn)
					{
						//if (sounds_on == 1) al_play_sample(sound_step, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						hero_x += nh_mvn;
					}

					redraw = true;
				}

				if (light_points_current >= light_points_max)
				{
					if (key[KEY_W] && hero_y >= lh_mvn && hero_y >= 290)
					{
						hero_y -= lh_mvn;
					}

					if (key[KEY_S] && hero_y <= SCREEN_H - 157 - lh_mvn)
					{
						hero_y += lh_mvn;
					}

					if (key[KEY_A] && hero_x >= lh_mvn)
					{
						hero_x -= lh_mvn;
					}

					if (key[KEY_D] && hero_x <= SCREEN_W - 77 - lh_mvn)
					{
						hero_x += lh_mvn;
					}

					redraw = true;
				}

				if (dark_points_current >= dark_points_max)
				{
					if (key[KEY_W] && hero_y >= dh_mvn && hero_y >= 290)
					{
						hero_y -= dh_mvn;
					}

					if (key[KEY_S] && hero_y <= SCREEN_H - 157 - dh_mvn)
					{
						hero_y += dh_mvn;
					}

					if (key[KEY_A] && hero_x >= dh_mvn)
					{
						hero_x -= dh_mvn;
					}

					if (key[KEY_D] && hero_x <= SCREEN_W - 77 - dh_mvn)
					{
						hero_x += dh_mvn;
					}

					redraw = true;
				}

			}

			/// Hero Attack 
			if (ev.type == ALLEGRO_EVENT_TIMER)
			{
				if (key[KEY_J] == true && shoot_arrow == 0)
				{
					if (sounds_on == 1 && light_points_current < light_points_max && dark_points_current < dark_points_max) al_play_sample(sound_shoot, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (sounds_on == 1 && light_points_current >= light_points_max || sounds_on == 1 && dark_points_current >= dark_points_max) al_play_sample(sound_light_j, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					shoot_arrow = 1;
					arrow_x = hero_x;
					arrow_y = hero_y;
				}

				if (key[KEY_K] == true && shoot_heal == 0)
				{
					if (sounds_on == 1 && light_points_current < light_points_max && dark_points_current < dark_points_max) al_play_sample(sound_shoot, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (sounds_on == 1 && light_points_current >= light_points_max || sounds_on == 1 && dark_points_current >= dark_points_max) al_play_sample(sound_light_k, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					shoot_heal = 1;
					heal_x = hero_x;
					heal_y = hero_y;
				}

				if (key[KEY_L] == true && shoot_ultimate == 0)
				{
					shoot_ultimate = 1;
					if (sounds_on == 1 && light_points_current >= light_points_max || sounds_on == 1 && dark_points_current >= dark_points_max) al_play_sample(sound_ultimate, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					//ultimate_x = hero_x;
					//ultimate_y = hero_y;
				}


			}

			/// Keys Checker
			else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
			{
				switch (ev.keyboard.keycode) {
				case ALLEGRO_KEY_W:
					key[KEY_W] = true;
					break;

				case ALLEGRO_KEY_S:
					key[KEY_S] = true;
					break;

				case ALLEGRO_KEY_A:
					key[KEY_A] = true;
					break;

				case ALLEGRO_KEY_D:
					key[KEY_D] = true;
					break;

				case ALLEGRO_KEY_J:
					key[KEY_J] = true;
					break;

				case ALLEGRO_KEY_K:
					key[KEY_K] = true;
					break;

				case ALLEGRO_KEY_L:
					key[KEY_L] = true;
					break;
				}
			}
			else if (ev.type == ALLEGRO_EVENT_KEY_UP)
			{
				switch (ev.keyboard.keycode)
				{
				case ALLEGRO_KEY_W:
					key[KEY_W] = false;
					break;

				case ALLEGRO_KEY_S:
					key[KEY_S] = false;
					break;

				case ALLEGRO_KEY_A:
					key[KEY_A] = false;
					break;

				case ALLEGRO_KEY_D:
					key[KEY_D] = false;
					break;

				case ALLEGRO_KEY_J:
					key[KEY_J] = false;
					break;

				case ALLEGRO_KEY_K:
					key[KEY_K] = false;
					break;

				case ALLEGRO_KEY_L:
					key[KEY_L] = false;
					break;


					//case ALLEGRO_KEY_ESCAPE:
					//doexit = true;
					//break;
				}
			}

			/// Ultimate Skill Cooldown
			if (light_points_current >= light_points_max)
			{
				cooldown_ultimate = lh_cd;
				if (10 * frameCount / 600 % cooldown_ultimate == 0 && shoot_ultimate == 2)
				{
					shoot_ultimate = 0;
				}
			}

			if (dark_points_current >= dark_points_max)
			{
				cooldown_ultimate = dh_cd;
				if (10 * frameCount / 600 % cooldown_ultimate == 0 && shoot_ultimate == 2)
				{
					shoot_ultimate = 0;
				}
			}

			if (10 * frameCount / 600 % 3 == 0 && shoot_ultimate == 1)
			{
				shoot_ultimate = 2;
			}

			if (10 * frameCount / 600 % 3 == 0) al_stop_sample(sound_ultimate);

			/// C O L L I S I O N S

			/// Collisions Zombie 1
			if (zombie_current_hp > 0 && zombie_current_hp < ze_hp && zombie_density > 0)
			{
				/// Arrow - Normal Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_arrow == 1 && zombie_alive == 1 && arrow_x >= zombie_x && arrow_x < zombie_x + 85)
					{
						if (arrow_y + 67 >= zombie_y && arrow_y + 67 < zombie_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_damaged, zombie_x, zombie_y, 0);
							shoot_arrow = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) zombie_current_hp -= nh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0 ) dark_points_current -= morality;
						}
					}
				}

				/// Arrow - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_arrow == 1 && zombie_alive == 1 && arrow_x + 90 >= zombie_x && arrow_x + 90 < zombie_x + 85)
					{
						if (arrow_y + 67 >= zombie_y && arrow_y + 67 < zombie_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_damaged, zombie_x, zombie_y, 0);
							shoot_arrow = 0;
							if (light_points_current >= light_points_max) zombie_current_hp -= lh_dmg;
							if (dark_points_current >= dark_points_max) zombie_current_hp -= dh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
						}
					}
				}

				/// Heal - Noraml Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_heal == 1 && zombie_alive == 1 && heal_x - 30 >= zombie_x && heal_x - 30 < zombie_x + 85)
					{
						if (heal_y + 66 >= zombie_y && heal_y + 66 < zombie_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_healed, zombie_x, zombie_y, 0);
							shoot_heal = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) zombie_current_hp += nh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality;
						}
					}
				}

				/// Heal - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_heal == 1 && zombie_alive == 1 && heal_x - 30 + 90 >= zombie_x && heal_x - 30 + 90 < zombie_x + 85)
					{
						if (heal_y + 66 >= zombie_y && heal_y + 66 < zombie_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_healed, zombie_x, zombie_y, 0);
							shoot_heal = 0;
							if (light_points_current >= light_points_max) zombie_current_hp += lh_dmg;
							if (dark_points_current >= dark_points_max) zombie_current_hp += dh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality * game_level;
						}
					}
				}

				/// Ultimate Collision
				if (shoot_ultimate == 1 && zombie_current_hp < ze_hp && zombie_x <= 1300)
				{
					if (hero_y + 66 >= zombie_y && hero_y + 66 < zombie_y + 151)
					{
						if (light_points_current >= light_points_max) 
						{
							al_draw_bitmap(zombie_collision_healed, zombie_x, zombie_y, 0);
							zombie_current_hp += lh_dmg * lh_dmg;
							dark_points_current += morality * game_level;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

						if (dark_points_current >= dark_points_max)
						{
							al_draw_bitmap(zombie_collision_damaged, zombie_x, zombie_y, 0);
							zombie_current_hp -= dh_dmg * dh_dmg;
							light_points_current += morality * game_level;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

					}
				}
			}

			/// Collisions Zombie 2
			if (zombie2_current_hp > 0 && zombie2_current_hp < ze_hp && zombie_density > 0)
			{
				/// Arrow - Normal Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_arrow == 1 && zombie2_alive == 1 && arrow_x >= zombie2_x && arrow_x < zombie2_x + 85)
					{
						if (arrow_y + 67 >= zombie2_y && arrow_y + 67 < zombie2_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_damaged, zombie2_x, zombie2_y, 0);
							shoot_arrow = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) zombie2_current_hp -= nh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality;
						}
					}
				}

				/// Arrow - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_arrow == 1 && zombie2_alive == 1 && arrow_x + 90 >= zombie2_x && arrow_x + 90 < zombie2_x + 85)
					{
						if (arrow_y + 67 >= zombie2_y && arrow_y + 67 < zombie2_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_damaged, zombie2_x, zombie2_y, 0);
							shoot_arrow = 0;
							if (light_points_current >= light_points_max) zombie2_current_hp -= lh_dmg;
							if (dark_points_current >= dark_points_max) zombie2_current_hp -= dh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
						}
					}
				}

				/// Heal - Noraml Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_heal == 1 && zombie2_alive == 1 && heal_x - 30 >= zombie2_x && heal_x - 30 < zombie2_x + 85)
					{
						if (heal_y + 66 >= zombie2_y && heal_y + 66 < zombie2_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_healed, zombie2_x, zombie2_y, 0);
							shoot_heal = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) zombie2_current_hp += nh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality;
						}
					}
				}

				/// Heal - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_heal == 1 && zombie2_alive == 1 && heal_x - 30 + 90 >= zombie2_x && heal_x - 30 + 90 < zombie2_x + 85)
					{
						if (heal_y + 66 >= zombie2_y && heal_y + 66 < zombie2_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_healed, zombie2_x, zombie2_y, 0);
							shoot_heal = 0;
							if (light_points_current >= light_points_max) zombie2_current_hp += lh_dmg;
							if (dark_points_current >= dark_points_max) zombie2_current_hp += dh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality * game_level;
						}
					}
				}

				/// Ultimate Collision
				if (shoot_ultimate == 1 && zombie2_current_hp < ze_hp && zombie2_x <= 1300)
				{
					if (hero_y + 66 >= zombie2_y && hero_y + 66 < zombie2_y + 151)
					{
						if (light_points_current >= light_points_max)
						{
							al_draw_bitmap(zombie_collision_healed, zombie2_x, zombie2_y, 0);
							zombie2_current_hp += lh_dmg * lh_dmg;
							dark_points_current += morality * game_level;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

						if (dark_points_current >= dark_points_max)
						{
							al_draw_bitmap(zombie_collision_damaged, zombie2_x, zombie2_y, 0);
							zombie2_current_hp -= dh_dmg * dh_dmg;
							light_points_current += morality * game_level;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

					}
				}
			}

			/// Collisions Zombie 3
			if (zombie3_current_hp > 0 && zombie3_current_hp < ze_hp && zombie_density > 0)
			{
				/// Arrow - Normal Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_arrow == 1 && zombie3_alive == 1 && arrow_x >= zombie3_x && arrow_x < zombie3_x + 85)
					{
						if (arrow_y + 67 >= zombie3_y && arrow_y + 67 < zombie3_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_damaged, zombie3_x, zombie3_y, 0);
							shoot_arrow = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) zombie3_current_hp -= nh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality;
						}
					}
				}

				/// Arrow - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_arrow == 1 && zombie3_alive == 1 && arrow_x + 90 >= zombie3_x && arrow_x + 90 < zombie3_x + 85)
					{
						if (arrow_y + 67 >= zombie3_y && arrow_y + 67 < zombie3_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_damaged, zombie3_x, zombie3_y, 0);
							shoot_arrow = 0;
							if (light_points_current >= light_points_max) zombie3_current_hp -= lh_dmg;
							if (dark_points_current >= dark_points_max) zombie3_current_hp -= dh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
						}
					}
				}

				/// Heal - Noraml Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_heal == 1 && zombie3_alive == 1 && heal_x - 30 >= zombie3_x && heal_x - 30 < zombie3_x + 85)
					{
						if (heal_y + 66 >= zombie3_y && heal_y + 66 < zombie3_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_healed, zombie3_x, zombie3_y, 0);
							shoot_heal = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) zombie3_current_hp += nh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality;
						}
					}
				}

				/// Heal - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_heal == 1 && zombie3_alive == 1 && heal_x - 30 + 90 >= zombie3_x && heal_x - 30 + 90 < zombie3_x + 85)
					{
						if (heal_y + 66 >= zombie3_y && heal_y + 66 < zombie3_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_healed, zombie3_x, zombie3_y, 0);
							shoot_heal = 0;
							if (light_points_current >= light_points_max) zombie3_current_hp += lh_dmg;
							if (dark_points_current >= dark_points_max) zombie3_current_hp += dh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality * game_level;
						}
					}
				}

				/// Ultimate Collision
				if (shoot_ultimate == 1 && zombie3_current_hp < ze_hp && zombie3_x <= 1300)
				{
					if (hero_y + 66 >= zombie3_y && hero_y + 66 < zombie3_y + 151)
					{
						if (light_points_current >= light_points_max)
						{
							al_draw_bitmap(zombie_collision_healed, zombie3_x, zombie3_y, 0);
							zombie3_current_hp += lh_dmg * lh_dmg;
							dark_points_current += morality * game_level;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

						if (dark_points_current >= dark_points_max)
						{
							al_draw_bitmap(zombie_collision_damaged, zombie3_x, zombie3_y, 0);
							zombie3_current_hp -= dh_dmg * dh_dmg;
							light_points_current += morality * game_level;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

					}
				}
			}

			/// Collisions Zombie 4
			if (zombie4_current_hp > 0 && zombie4_current_hp < ze_hp && zombie_density > 0)
			{
				/// Arrow - Normal Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_arrow == 1 && zombie4_alive == 1 && arrow_x >= zombie4_x && arrow_x < zombie4_x + 85)
					{
						if (arrow_y + 67 >= zombie4_y && arrow_y + 67 < zombie4_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_damaged, zombie4_x, zombie4_y, 0);
							shoot_arrow = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) zombie4_current_hp -= nh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality;
						}
					}
				}

				/// Arrow - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_arrow == 1 && zombie4_alive == 1 && arrow_x + 90 >= zombie4_x && arrow_x + 90 < zombie4_x + 85)
					{
						if (arrow_y + 67 >= zombie4_y && arrow_y + 67 < zombie4_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_damaged, zombie4_x, zombie4_y, 0);
							shoot_arrow = 0;
							if (light_points_current >= light_points_max) zombie4_current_hp -= lh_dmg;
							if (dark_points_current >= dark_points_max) zombie4_current_hp -= dh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
						}
					}
				}

				/// Heal - Noraml Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_heal == 1 && zombie4_alive == 1 && heal_x - 30 >= zombie4_x && heal_x - 30 < zombie4_x + 85)
					{
						if (heal_y + 66 >= zombie4_y && heal_y + 66 < zombie4_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_healed, zombie4_x, zombie4_y, 0);
							shoot_heal = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) zombie4_current_hp += nh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality;
						}
					}
				}

				/// Heal - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_heal == 1 && zombie4_alive == 1 && heal_x - 30 + 90 >= zombie4_x && heal_x - 30 + 90 < zombie4_x + 85)
					{
						if (heal_y + 66 >= zombie4_y && heal_y + 66 < zombie4_y + 151)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(zombie_collision_healed, zombie4_x, zombie4_y, 0);
							shoot_heal = 0;
							if (light_points_current >= light_points_max) zombie4_current_hp += lh_dmg;
							if (dark_points_current >= dark_points_max) zombie4_current_hp += dh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality * game_level;
						}
					}
				}

				/// Ultimate Collision
				if (shoot_ultimate == 1 && zombie4_current_hp < ze_hp && zombie4_x <= 1300)
				{
					if (hero_y + 66 >= zombie4_y && hero_y + 66 < zombie4_y + 151)
					{
						if (light_points_current >= light_points_max)
						{
							al_draw_bitmap(zombie_collision_healed, zombie4_x, zombie4_y, 0);
							zombie4_current_hp += lh_dmg * lh_dmg;
							dark_points_current += morality * game_level;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

						if (dark_points_current >= dark_points_max)
						{
							al_draw_bitmap(zombie_collision_damaged, zombie4_x, zombie4_y, 0);
							zombie4_current_hp -= dh_dmg * dh_dmg;
							light_points_current += morality * game_level;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

					}
				}
			}

			/// Collisions Human 1
			if (human_current_hp > 0 && human_current_hp < he_hp && human_density > 0)
			{
				/// Arrow - Normal Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_arrow == 1 && human_alive == 1 && arrow_x + 30 >= human_x && arrow_x + 30 < human_x + 45) 
					{
						if (arrow_y + 65 >= human_y && arrow_y + 65 < human_y + 160)
						{ 
							if (sounds_on == 1) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_damaged, human_x, human_y, 0);
							shoot_arrow = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) human_current_hp -= nh_dmg;
							if (light_points_current > 0) light_points_current -= morality;
							dark_points_current += morality;
						}
					}
				}

				/// Arrow - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_arrow == 1 && human_alive == 1 && arrow_x + 90 + 35 >= human_x && arrow_x + 90 + 35 < human_x + 48) 
					{
						if (arrow_y + 65 >= human_y && arrow_y + 65 < human_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_damaged, human_x, human_y, 0);
							shoot_arrow = 0;
							if (light_points_current >= light_points_max) human_current_hp -= lh_dmg;
							if (dark_points_current >= dark_points_max) human_current_hp -= dh_dmg;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							dark_points_current += morality;
						}
					}
				}

				/// Heal - Noraml Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max) 
				{
					if (shoot_heal == 1 && human_alive == 1 && heal_x - 0 >= human_x && heal_x - 0 < human_x + 48) 
					{
						if (heal_y + 65 >= human_y && heal_y + 65 < human_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_heal, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_healed, human_x, human_y, 0);
							shoot_heal = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) human_current_hp += nh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality;
						}
					}
				}

				/// Heal - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_heal == 1 && human_alive == 1 && heal_x + 28 + 90 >= human_x && heal_x + 28 + 90 < human_x + 48)
					{
						if (heal_y + 65 >= human_y && heal_y + 65 < human_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_heal, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_healed, human_x, human_y, 0);
							shoot_heal = 0;
							if (light_points_current >= light_points_max) human_current_hp += lh_dmg;
							if (dark_points_current >= dark_points_max) human_current_hp += dh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
						}
					}
				}

				/// Ultimate Collision
				if (shoot_ultimate == 1 && human_current_hp < he_hp && human_x <= 1300)
				{
					if (hero_y + 65 >= human_y && hero_y + 65 < human_y + 160)
					{
						if (light_points_current >= light_points_max)
						{
							al_draw_bitmap(human_collision_healed, human_x, human_y, 0);
							human_current_hp += lh_dmg * lh_dmg;
							light_points_current += morality * game_level;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

						if (dark_points_current >= dark_points_max)
						{
							al_draw_bitmap(human_collision_damaged, human_x, human_y, 0);
							human_current_hp -= dh_dmg * dh_dmg;
							dark_points_current += morality * game_level;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

					}
				}
			}

			/// Collisions Human 2
			if (human2_current_hp > 0 && human2_current_hp < he_hp && human_density > 0)
			{
				/// Arrow - Normal Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_arrow == 1 && human2_alive == 1 && arrow_x + 30 >= human2_x && arrow_x + 30 < human2_x + 45)
					{
						if (arrow_y + 65 >= human2_y && arrow_y + 65 < human2_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_damaged, human2_x, human2_y, 0);
							shoot_arrow = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) human2_current_hp -= nh_dmg;
							if (light_points_current > 0) light_points_current -= morality;
							dark_points_current += morality;
						}
					}
				}

				/// Arrow - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_arrow == 1 && human2_alive == 1 && arrow_x + 90 + 35 >= human2_x && arrow_x + 90 + 35 < human2_x + 48)
					{
						if (arrow_y + 65 >= human2_y && arrow_y + 65 < human2_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_damaged, human2_x, human2_y, 0);
							shoot_arrow = 0;
							if (light_points_current >= light_points_max) human2_current_hp -= lh_dmg;
							if (dark_points_current >= dark_points_max) human2_current_hp -= dh_dmg;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							dark_points_current += morality;
						}
					}
				}

				/// Heal - Noraml Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_heal == 1 && human2_alive == 1 && heal_x - 0 >= human2_x && heal_x - 0 < human2_x + 48)
					{
						if (heal_y + 65 >= human2_y && heal_y + 65 < human2_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_heal, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_healed, human2_x, human2_y, 0);
							shoot_heal = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) human2_current_hp += nh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality;
						}
					}
				}

				/// Heal - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_heal == 1 && human2_alive == 1 && heal_x + 28 + 90 >= human2_x && heal_x + 28 + 90 < human2_x + 48)
					{
						if (heal_y + 65 >= human2_y && heal_y + 65 < human2_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_heal, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_healed, human2_x, human2_y, 0);
							shoot_heal = 0;
							if (light_points_current >= light_points_max) human2_current_hp += lh_dmg;
							if (dark_points_current >= dark_points_max) human2_current_hp += dh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
						}
					}
				}

				/// Ultimate Collision
				if (shoot_ultimate == 1 && human2_current_hp < he_hp && human2_x <= 1300)
				{
					if (hero_y + 65 >= human2_y && hero_y + 65 < human2_y + 160)
					{
						if (light_points_current >= light_points_max)
						{
							al_draw_bitmap(human_collision_healed, human2_x, human2_y, 0);
							human2_current_hp += lh_dmg * lh_dmg;
							light_points_current += morality * game_level;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

						if (dark_points_current >= dark_points_max)
						{
							al_draw_bitmap(human_collision_damaged, human2_x, human2_y, 0);
							human2_current_hp -= dh_dmg * dh_dmg;
							dark_points_current += morality * game_level;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

					}
				}
			}

			/// Collisions Human 3
			if (human3_current_hp > 0 && human3_current_hp < he_hp && human_density > 0)
			{
				/// Arrow - Normal Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_arrow == 1 && human3_alive == 1 && arrow_x + 30 >= human3_x && arrow_x + 30 < human3_x + 45)
					{
						if (arrow_y + 65 >= human3_y && arrow_y + 65 < human3_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_damaged, human3_x, human3_y, 0);
							shoot_arrow = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) human3_current_hp -= nh_dmg;
							if (light_points_current > 0) light_points_current -= morality;
							dark_points_current += morality;
						}
					}
				}

				/// Arrow - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_arrow == 1 && human3_alive == 1 && arrow_x + 90 + 35 >= human3_x && arrow_x + 90 + 35 < human3_x + 48)
					{
						if (arrow_y + 65 >= human3_y && arrow_y + 65 < human3_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_damaged, human3_x, human3_y, 0);
							shoot_arrow = 0;
							if (light_points_current >= light_points_max) human3_current_hp -= lh_dmg;
							if (dark_points_current >= dark_points_max) human3_current_hp -= dh_dmg;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							dark_points_current += morality;
						}
					}
				}

				/// Heal - Noraml Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_heal == 1 && human3_alive == 1 && heal_x - 0 >= human3_x && heal_x - 0 < human3_x + 48)
					{
						if (heal_y + 65 >= human3_y && heal_y + 65 < human3_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_heal, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_healed, human3_x, human3_y, 0);
							shoot_heal = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) human3_current_hp += nh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality;
						}
					}
				}

				/// Heal - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_heal == 1 && human3_alive == 1 && heal_x + 28 + 90 >= human3_x && heal_x + 28 + 90 < human3_x + 48)
					{
						if (heal_y + 65 >= human3_y && heal_y + 65 < human3_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_heal, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_healed, human3_x, human3_y, 0);
							shoot_heal = 0;
							if (light_points_current >= light_points_max) human3_current_hp += lh_dmg;
							if (dark_points_current >= dark_points_max) human3_current_hp += dh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
						}
					}
				}

				/// Ultimate Collision
				if (shoot_ultimate == 1 && human3_current_hp < he_hp && human3_x <= 1300)
				{
					if (hero_y + 65 >= human3_y && hero_y + 65 < human3_y + 160)
					{
						if (light_points_current >= light_points_max)
						{
							al_draw_bitmap(human_collision_healed, human3_x, human3_y, 0);
							human3_current_hp += lh_dmg * lh_dmg;
							light_points_current += morality * game_level;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

						if (dark_points_current >= dark_points_max)
						{
							al_draw_bitmap(human_collision_damaged, human3_x, human3_y, 0);
							human3_current_hp -= dh_dmg * dh_dmg;
							dark_points_current += morality * game_level;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

					}
				}
			}

			/// Collisions Human 4
			if (human4_current_hp > 0 && human4_current_hp < he_hp && human_density > 0)
			{
				/// Arrow - Normal Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_arrow == 1 && human4_alive == 1 && arrow_x + 30 >= human4_x && arrow_x + 30 < human4_x + 45)
					{
						if (arrow_y + 65 >= human4_y && arrow_y + 65 < human4_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_damaged, human4_x, human4_y, 0);
							shoot_arrow = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) human4_current_hp -= nh_dmg;
							if (light_points_current > 0) light_points_current -= morality;
							dark_points_current += morality;
						}
					}
				}

				/// Arrow - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_arrow == 1 && human4_alive == 1 && arrow_x + 90 + 35 >= human4_x && arrow_x + 90 + 35 < human4_x + 48)
					{
						if (arrow_y + 65 >= human4_y && arrow_y + 65 < human4_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_damaged, human4_x, human4_y, 0);
							shoot_arrow = 0;
							if (light_points_current >= light_points_max) human4_current_hp -= lh_dmg;
							if (dark_points_current >= dark_points_max) human4_current_hp -= dh_dmg;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							dark_points_current += morality;
						}
					}
				}

				/// Heal - Noraml Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_heal == 1 && human4_alive == 1 && heal_x - 0 >= human4_x && heal_x - 0 < human4_x + 48)
					{
						if (heal_y + 65 >= human4_y && heal_y + 65 < human4_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_heal, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_healed, human4_x, human4_y, 0);
							shoot_heal = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) human4_current_hp += nh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality;
						}
					}
				}

				/// Heal - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_heal == 1 && human4_alive == 1 && heal_x + 28 + 90 >= human4_x && heal_x + 28 + 90 < human4_x + 48)
					{
						if (heal_y + 65 >= human4_y && heal_y + 65 < human4_y + 160)
						{
							if (sounds_on == 1) al_play_sample(sound_heal, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(human_collision_healed, human4_x, human4_y, 0);
							shoot_heal = 0;
							if (light_points_current >= light_points_max) human4_current_hp += lh_dmg;
							if (dark_points_current >= dark_points_max) human4_current_hp += dh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
						}
					}
				}

				/// Ultimate Collision
				if (shoot_ultimate == 1 && human4_current_hp < he_hp && human4_x <= 1300)
				{
					if (hero_y + 65 >= human4_y && hero_y + 65 < human4_y + 160)
					{
						if (light_points_current >= light_points_max)
						{
							al_draw_bitmap(human_collision_healed, human4_x, human4_y, 0);
							human4_current_hp += lh_dmg * lh_dmg;
							light_points_current += morality * game_level;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

						if (dark_points_current >= dark_points_max)
						{
							al_draw_bitmap(human_collision_damaged, human4_x, human4_y, 0);
							human4_current_hp -= dh_dmg * dh_dmg;
							dark_points_current += morality * game_level;
							if (light_points_current > 0) light_points_current -= morality * game_level;
							shoot_ultimate = 1;
						}

					}
				}
			}

			/// Collisions Hero - Boss 
			if (boss_current_hp > 0 && boss_current_hp < be_hp)
			{
				/// Arrow - Normal Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_arrow == 1 && boss_alive == 1 && arrow_x - 135 >= boss_x && arrow_x - 135 < boss_x + 85)
					{
						if (arrow_y + 67 >= boss_y && arrow_y + 67 < boss_y + 622)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(boss_collision_damaged, boss_x, boss_y, 0);
							shoot_arrow = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) boss_current_hp -= nh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality;
						}
					}
				}

				/// Arrow - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_arrow == 1 && boss_alive == 1 && arrow_x + 90 - 120 >= boss_x && arrow_x + 90 - 120 < boss_x + 85)
					{
						if (arrow_y + 67 >= boss_y && arrow_y + 67 < boss_y + 622)
						{
							if (sounds_on == 1) al_play_sample(sound_zombie_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(boss_collision_damaged, boss_x, boss_y, 0);
							shoot_arrow = 0;
							if (light_points_current >= light_points_max) boss_current_hp -= lh_dmg;
							if (dark_points_current >= dark_points_max) boss_current_hp -= dh_dmg;
							light_points_current += morality;
							if (dark_points_current > 0) dark_points_current -= morality * game_level;
						}
					}
				}

				/// Heal - Noraml Hero Collision
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (shoot_heal == 1 && boss_alive == 1 && heal_x - 30 - 135 >= boss_x && heal_x - 30 - 135 < boss_x + 85)
					{
						if (heal_y + 66 >= boss_y && heal_y + 66 < boss_y + 622)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(boss_collision_healed, boss_x, boss_y, 0);
							shoot_heal = 0;
							if (light_points_current < light_points_max && dark_points_current < dark_points_max) boss_current_hp += nh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality;
						}
					}
				}

				/// Heal - Light/Dark Hero Collision
				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (shoot_heal == 1 && boss_alive == 1 && heal_x - 30 + 90 - 120 >= boss_x && heal_x - 30 + 90 - 120 < boss_x + 85)
					{
						if (heal_y + 66 >= boss_y && heal_y + 66 < boss_y + 622)
						{
							if (sounds_on == 1) al_play_sample(sound_heal2, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
							al_draw_bitmap(boss_collision_healed, boss_x, boss_y, 0);
							shoot_heal = 0;
							if (light_points_current >= light_points_max) boss_current_hp += lh_dmg;
							if (dark_points_current >= dark_points_max) boss_current_hp += dh_dmg;
							dark_points_current += morality;
							if (light_points_current > 0) light_points_current -= morality * game_level;
						}
					}
				}

				/// Ultimate Collision
				if (shoot_ultimate == 1 && boss_current_hp < be_hp && boss_x <= 1300)
				{
					if (hero_y + 66 >= boss_y && hero_y + 66 < boss_y + 622)
					{
						if (light_points_current >= light_points_max)
						{
							//al_draw_bitmap(boss_collision_healed, boss_x, boss_y, 0);
							//boss_current_hp += lh_dmg * lh_dmg;
							//dark_points_current += morality * game_level;
							//if (light_points_current > 0) light_points_current -= morality * game_level;
							//shoot_ultimate = 0;
						}

						if (dark_points_current >= dark_points_max)
						{
							//al_draw_bitmap(boss_collision_damaged, boss_x, boss_y, 0);
							//boss_current_hp -= dh_dmg * dh_dmg;
							//light_points_current += morality * game_level;
							//if (dark_points_current > 0) dark_points_current -= morality * game_level;
							//shoot_ultimate = 0;
						}

					}
				}
			}

			/// Collision Boss - Hero
			if (boss_current_hp > 0 && boss_current_hp < be_hp && boss_x <= -80)
			{
				if (sounds_on == 1) al_play_sample(sound_hero_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				hero_current_hp = 0;
			}

			/// Collisions Audio 
			if (sounds_on == 1)
			{
				if (zombie_x >= -85 && zombie_alive == 1 && zombie_current_hp > 0 && zombie_x <= -66 && zombie_x > -67 && zombie_current_hp < ze_hp) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				if (zombie2_x >= -85 && zombie2_alive == 1 && zombie2_current_hp > 0 && zombie2_x <= -66 && zombie2_x > -67 && zombie2_current_hp < ze_hp) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				if (zombie3_x >= -85 && zombie3_alive == 1 && zombie3_current_hp > 0 && zombie3_x <= -66 && zombie3_x > -67 && zombie3_current_hp < ze_hp) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				if (zombie4_x >= -85 && zombie4_alive == 1 && zombie4_current_hp > 0 && zombie4_x <= -66 && zombie4_x > -67 && zombie4_current_hp < ze_hp) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

				if (human_x >= -85 && human_alive == 1 && human_current_hp > 0 && human_x <= -66 && human_x > -67 && human_current_hp < he_hp) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				if (human2_x >= -85 && human2_alive == 1 && human2_current_hp > 0 && human2_x <= -66 && human2_x > -67 && human2_current_hp < he_hp) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				if (human3_x >= -85 && human3_alive == 1 && human3_current_hp > 0 && human3_x <= -66 && human3_x > -67 && human3_current_hp < he_hp) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				if (human4_x >= -85 && human4_alive == 1 && human4_current_hp > 0 && human4_x <= -66 && human4_x > -67 && human4_current_hp < he_hp) al_play_sample(sound_human_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			}

			/// Boss Spawn
			if (boss_density > 0)
			{
				if (game_time < boss_time)
				{
					zombie_density = 4;
					human_density = 4;
					boss_alive = -1;
				}

				if (game_time >= boss_time - 1 && game_time <= boss_time)
				{
					if (zombie_current_hp > 0)  al_draw_bitmap (zombie_collision_damaged, zombie_x, zombie_y, 0);
					if (zombie2_current_hp > 0) al_draw_bitmap (zombie_collision_damaged, zombie2_x, zombie2_y, 0);
					if (zombie3_current_hp > 0) al_draw_bitmap (zombie_collision_damaged, zombie3_x, zombie3_y, 0);
					if (zombie4_current_hp > 0) al_draw_bitmap (zombie_collision_damaged, zombie4_x, zombie4_y, 0);
					if (human_current_hp > 0)  al_draw_bitmap (human_collision_damaged, human_x, human_y, 0);
					if (human2_current_hp > 0) al_draw_bitmap (human_collision_damaged, human2_x, human2_y, 0);
					if (human3_current_hp > 0) al_draw_bitmap (human_collision_damaged, human3_x, human3_y, 0);
					if (human4_current_hp > 0) al_draw_bitmap (human_collision_damaged, human4_x, human4_y, 0);
				}
				 
				if (game_time >= boss_time)
				{
					boss_alive = 1;
					zombie_density = 0;
					human_density = 0;
				}
			}

			/// Boss HP Bar
			al_draw_filled_rectangle(100 + boss_x, boss_y - 15, 100 + boss_x + 2.1 * 100, boss_y - 25, color_blackk);
			al_draw_filled_rectangle(100 + boss_x, boss_y - 15, 100 + boss_x + 2.1 * percent_boss_hp, boss_y - 25, color_red);

			/// Hero Animation
			if (redraw && hero_current_hp > 0)
			{
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (hero_current_hp > nh_hp / 2)
					{
						if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(hero_noblood_1, hero_x, hero_y, 0);
						if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(hero_noblood_2, hero_x, hero_y, 0);
					}

					if (hero_current_hp <= nh_hp / 2)
					{
						if (frameCount / character_animation_slow % 2 == 0) al_draw_bitmap(hero_blood_1, hero_x, hero_y, 0);
						if (frameCount / character_animation_slow % 2 == 1) al_draw_bitmap(hero_blood_2, hero_x, hero_y, 0);
					}
				}

				if (light_points_current >= light_points_max)
				{
					if (frameCount / character_levitation_slow % 10 == 0) al_draw_bitmap(light_1, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 1) al_draw_bitmap(light_2, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 2) al_draw_bitmap(light_3, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 3) al_draw_bitmap(light_4, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 4) al_draw_bitmap(light_5, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 5) al_draw_bitmap(light_5, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 6) al_draw_bitmap(light_4, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 7) al_draw_bitmap(light_3, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 8) al_draw_bitmap(light_2, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 9) al_draw_bitmap(light_1, hero_x, hero_y, 0);
				}

				if (dark_points_current >= dark_points_max)
				{
					if (frameCount / character_levitation_slow % 10 == 0) al_draw_bitmap(dark_1, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 1) al_draw_bitmap(dark_2, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 2) al_draw_bitmap(dark_3, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 3) al_draw_bitmap(dark_4, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 4) al_draw_bitmap(dark_5, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 5) al_draw_bitmap(dark_5, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 6) al_draw_bitmap(dark_4, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 7) al_draw_bitmap(dark_3, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 8) al_draw_bitmap(dark_2, hero_x, hero_y, 0);
					if (frameCount / character_levitation_slow % 10 == 9) al_draw_bitmap(dark_1, hero_x, hero_y, 0);
				}

			}
		
			/// Collisions Fireball 
			if (boss_alive > 0 && fireball_alive == 1 && boss_current_hp > 0)
			{
				if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				{
					if (fireball_x + 25 >= hero_x + 4 && fireball_x < hero_x + 27 && fireball_y + 25 >= hero_y && fireball_y < hero_y + 157)
					{
						if (sounds_on == 1) al_play_sample(sound_hero_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						al_draw_bitmap(hero_collision_damaged, hero_x, hero_y, 0);
						hero_current_hp -= be_dmg;
						fireball_alive = 0;
					}

				}

				if (light_points_current >= light_points_max || dark_points_current >= dark_points_max)
				{
					if (fireball_x + 25 > hero_x + 25 && fireball_x <= hero_x + 42 && fireball_y + 25 >= hero_y && fireball_y < hero_y + 177)
					{
						if (sounds_on == 1) al_play_sample(sound_hero_hit, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						al_draw_bitmap(superhero_collision_damaged, hero_x, hero_y, 0);
						hero_current_hp -= be_dmg;
						fireball_alive = 0;
					}

				}


			}

			/// In Game Smoke
			if (animation_on == 1)
			{
				//menu_time = 1.66666666666*frameCount / 100;
				al_draw_bitmap(smoke, -30 * game_time, 0, 0);
				al_draw_bitmap(smoke, -30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 2 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 2 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 3 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 3 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 4 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 4 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 5 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 5 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 6 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 6 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 7 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 7 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 8 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 8 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 9 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 9 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 10 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 10 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 11 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 11 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 12 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 12 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 13 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 13 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 14 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 14 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 15 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 15 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 16 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 16 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 17 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 17 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 18 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 18 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 19 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 19 * 2276 - 30 * game_time, 0, 1);
				al_draw_bitmap(smoke, 20 * 2276 - 30 * game_time, 0, 0);
				al_draw_bitmap(smoke, 20 * 2276 - 30 * game_time, 0, 1);
			}
	}

		/// Percentage Counting
		if (light_points_current < light_points_max && dark_points_current < dark_points_max) percent_hp = hero_current_hp / nh_hp * 100;
		if (light_points_current >= light_points_max) percent_hp = hero_current_hp / lh_hp * 100;
		if (dark_points_current >= dark_points_max) percent_hp = hero_current_hp / dh_hp * 100;
	
		percent_boss_hp = boss_current_hp / be_hp * 100;

		percent_light_points = light_points_current / light_points_max * 100;
		percent_dark_points = dark_points_current / dark_points_max * 100;

		/// Morality Points Fix
		if (light_points_current < 0) light_points_current = 0;
		if (dark_points_current < 0) dark_points_current = 0;
		if (light_points_current >= morality_cap) light_points_current = morality_cap;
		if (dark_points_current >= morality_cap) dark_points_current = morality_cap;

		/// Game Screen Off
		if (game_screen_on == 1)
		{
			if (boss_current_hp <= 0 || boss_current_hp >= be_hp )
			{
				menu_screen_on = 0;
				game_screen_on = 0;
				tutorial_screen_on = 0;
				options_screen_on = 0;
				credits_screen_on = 0;
				victory_screen_on = 1;
				defeat_screen_on = 0;
			}

			if (hero_current_hp <= 0)
			{
				menu_screen_on = 0;
				game_screen_on = 0;
				tutorial_screen_on = 0;
				options_screen_on = 0;
				credits_screen_on = 0;
				victory_screen_on = 0;
				defeat_screen_on = 1;
			}
		}

		/// Interface On
		if (interface_on == 1 && game_screen_on == 1)
		{
			al_draw_bitmap(interface_background, 0, 0, 0);

			/// HP Bar
			if(hero_current_hp > 0 && percent_hp > 0)
			{
				al_draw_filled_rectangle(440, 904, 440 + 2.26 * percent_hp, 909, color_red);
				al_draw_filled_rectangle(440 + 2, 904 - 4, 440 + 2.26 * percent_hp - 2, 909 + 4, color_red);
				al_draw_filled_rectangle(440 + 2 + 2, 904 - 4 - 4, 440 + 2.26 * percent_hp - 2 - 2, 909 + 4 + 4, color_red);
			}

			/// Light Points Bar
			if (percent_light_points > 3 && light_points_current < light_points_max)
			{
				al_draw_filled_rectangle(440, 904 + 38, 440 + 2.26 * percent_light_points, 909 + 38, color_light);
				al_draw_filled_rectangle(440 + 2, 904 - 4 + 38, 440 + 2.26 * percent_light_points - 2, 909 + 4 + 38, color_light);
				al_draw_filled_rectangle(440 + 2 + 2, 904 - 4 - 4 + 38, 440 + 2.26 * percent_light_points - 2 - 2, 909 + 4 + 4 + 38, color_light);
			}

			if (light_points_current >= light_points_max)
			{
				al_draw_filled_rectangle(440, 904 + 38, 440 + 2.26 * 100, 909 + 38, color_light);
				al_draw_filled_rectangle(440 + 2, 904 - 4 + 38, 440 + 2.26 * 100 - 2, 909 + 4 + 38, color_light);
				al_draw_filled_rectangle(440 + 2 + 2, 904 - 4 - 4 + 38, 440 + 2.26 * 100 - 2 - 2, 909 + 4 + 4 + 38, color_light);
			}


			/// Dark Points Bar
			if (percent_dark_points > 3 && dark_points_current < dark_points_max)
			{
				al_draw_filled_rectangle(440, 904 + 38 + 38, 440 + 2.26 * percent_dark_points, 909 + 38 + 38, color_dark);
				al_draw_filled_rectangle(440 + 2, 904 - 4 + 38 + 38, 440 + 2.26 * percent_dark_points - 2, 909 + 4 + 38 + 38, color_dark);
				al_draw_filled_rectangle(440 + 2 + 2, 904 - 4 - 4 + 38 + 38, 440 + 2.26 * percent_dark_points - 2 - 2, 909 + 4 + 4 + 38 + 38, color_dark);
			}

			if (dark_points_current >= dark_points_max)
			{
				al_draw_filled_rectangle(440, 904 + 38 + 38, 440 + 2.26 * 100, 909 + 38 + 38, color_dark);
				al_draw_filled_rectangle(440 + 2, 904 - 4 + 38 + 38, 440 + 2.26 * 100 - 2, 909 + 4 + 38 + 38, color_dark);
				al_draw_filled_rectangle(440 + 2 + 2, 904 - 4 - 4 + 38 + 38, 440 + 2.26 * 100 - 2 - 2, 909 + 4 + 4 + 38 + 38, color_dark);
			}

			/// Button J Avaliable
			if (shoot_arrow == 0)
			{
				if (light_points_current < light_points_max && dark_points_current < dark_points_max) al_draw_bitmap(interface_j_normal, 0, 0, 0);
				if (light_points_current >= light_points_max) al_draw_bitmap(interface_j_light, 0, 0, 0);
				if (dark_points_current >= dark_points_max) al_draw_bitmap(interface_j_dark, 0, 0, 0);
			}

			/// Button K Avaliable
			if (shoot_heal == 0)
			{
				if (light_points_current < light_points_max && dark_points_current < dark_points_max) al_draw_bitmap(interface_k_normal, 0, 0, 0);
				if (light_points_current >= light_points_max) al_draw_bitmap(interface_k_light, 0, 0, 0);
				if (dark_points_current >= dark_points_max) al_draw_bitmap(interface_k_dark, 0, 0, 0);
			}

			/// Button K Avaliable
			if (shoot_ultimate == 0)
			{
				//if (light_points_current < light_points_max && dark_points_current < dark_points_max) al_draw_bitmap(interface_l_normal, 0, 0, 0);
				if (light_points_current >= light_points_max) al_draw_bitmap(interface_l_light, 0, 0, 0);
				if (dark_points_current >= dark_points_max) al_draw_bitmap(interface_l_dark, 0, 0, 0);
			}

			al_draw_bitmap(interface_frame, 0, 0, 0);

		}

		/// Victory Screen On
		if (victory_screen_on == 1)
		{
			menu_screen_on = 0;
			game_screen_on = 0;
			tutorial_screen_on = 0;
			options_screen_on = 0;
			credits_screen_on = 0;
			victory_screen_on = 1;
			defeat_screen_on = 0;
			al_draw_bitmap(screen_victory, 0, 0, 0);
			al_draw_bitmap(button_exit, 0, 0, 0);
			al_draw_bitmap(button_exit, 0, 0, 0);
			if (state.x <= 837 && state.x >= 442 && state.y <= 655 + 77 && state.y >= 655)
			{
				al_draw_bitmap(screen_score_continue, 0, 0, 0);
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
					if (game_screen_on == 0 && tutorial_screen_on == 0 && credits_screen_on == 0 && options_screen_on == 0 && defeat_screen_on == 0)
					{
						menu_screen_on = 0;
						game_screen_on = 1;
						tutorial_screen_on = 0;
						options_screen_on = 0;
						credits_screen_on = 0;
						victory_screen_on = 0;
						defeat_screen_on = 0;

						game_level++;
						frameCount = 0;

						set_stats();
						reset_enemy_position();
						reset_health_points();

						hero_current_hp = 100000;
						bool key[7] = { false, false, false, false, false, false, false };
					}
			}
		}

		/// Defeat Screen On
		if (defeat_screen_on == 1)
		{
			menu_screen_on = 0;
			game_screen_on = 0;
			tutorial_screen_on = 0;
			options_screen_on = 0;
			credits_screen_on = 0;
			victory_screen_on = 0;
			defeat_screen_on = 1;
			al_draw_bitmap(screen_defeat, 0, 0, 0);
			al_draw_bitmap(button_exit, 0, 0, 0);
			al_draw_bitmap(button_exit, 0, 0, 0);
			if (state.x <= 837 && state.x >= 442 && state.y <= 655 + 77 && state.y >= 655)
			{
				al_draw_bitmap(screen_score_continue, 0, 0, 0);
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
					if (game_screen_on == 0 && tutorial_screen_on == 0 && credits_screen_on == 0 && options_screen_on == 0 && victory_screen_on == 0)
					{
						menu_screen_on = 0;
						game_screen_on = 1;
						tutorial_screen_on = 0;
						options_screen_on = 0;
						credits_screen_on = 0;
						victory_screen_on = 0;
						defeat_screen_on = 0;
						if (game_level == 1)
						{
							game_level = 1;
							reset_stats();
						}

						frameCount = 0;
						
						set_stats();
						reset_enemy_position();
						reset_health_points();
						
						hero_current_hp = 100000;
						bool key[7] = { false, false, false, false, false, false, false };
					}
			}
		}

		/// Tutorial Screen On
		if (tutorial_screen_on == 1)
		{
			menu_screen_on = 0;
			game_screen_on = 0;
			options_screen_on = 0;
			credits_screen_on = 0;
			victory_screen_on = 0;
			defeat_screen_on = 0;
			al_draw_bitmap(screen_tutorial, 0, 0, 0);
			al_draw_bitmap(button_exit, 0, 0, 0);
			al_draw_bitmap(button_exit, 0, 0, 0);
		}

		/// Options Screen On
		if (options_screen_on == 1)
		{
			menu_screen_on = 0;
			game_screen_on = 0;
			tutorial_screen_on = 0;
			credits_screen_on = 0;
			victory_screen_on = 0;
			defeat_screen_on = 0;
			al_draw_bitmap(screen_options, 0, 0, 0);
			al_draw_bitmap(button_exit, 0, 0, 0);

			/// Music Volume Control
			al_draw_triangle(136, 424, 147, 402, 158, 424, color_white, 2);
			if (state.x <= 158 && state.x >= 136 && state.y <= 424 && state.y >= 402)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					music_volume += 0.05;
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				}

			al_draw_textf(couree_16, color_white, 147, 430, ALLEGRO_ALIGN_CENTRE, "%.0f", music_volume * 100);

			al_draw_triangle(136, 402 + 45, 158, 402 + 45, 147, 424 + 45, color_white, 2);
			if (state.x <= 158 && state.x >= 136 && state.y <= 424 + 45 && state.y >= 402 + 45)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					music_volume -= 0.05;
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				}

			if (music_volume > 1) music_volume = 1;
			if (music_volume < 0) music_volume = 0;

			/// Sound Volume Control
			al_draw_triangle(136, 424 + 90, 147, 402 + 90, 158, 424 + 90, color_white, 2);
			if (state.x <= 158 && state.x >= 136 && state.y <= 424 + 90 && state.y >= 402 + 90)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					sound_volume += 0.05;
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				}

			al_draw_textf(couree_16, color_white, 147, 430 + 90, ALLEGRO_ALIGN_CENTRE, "%.0f", sound_volume * 100);

			al_draw_triangle(136, 402 + 45 + 90, 158, 402 + 45 + 90, 147, 424 + 45 + 90, color_white, 2);
			if (state.x <= 158 && state.x >= 136 && state.y <= 424 + 45 + 90 && state.y >= 402 + 45 + 90)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					sound_volume -= 0.05;
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				}

			if (sound_volume > 1) sound_volume = 1;
			if (sound_volume < 0) sound_volume = 0;

			/// Audio Options
			if (music_on == 1 && state.x <= 241 && state.x >= 168 && state.y <= 476 && state.y >= 396)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) music_on = 2;
			if (music_on == 0 && state.x <= 241 && state.x >= 168 && state.y <= 476 && state.y >= 396)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) music_on = 1;

			if (sounds_on == 1 && state.x <= 241 && state.x >= 168 && state.y <= 562 && state.y >= 482)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) sounds_on = 2;
			if (sounds_on == 0 && state.x <= 241 && state.x >= 168 && state.y <= 562 && state.y >= 482)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) sounds_on = 1;


			if (state.x <= 241 && state.x >= 168 && state.y <= 476 && state.y >= 396)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (music_on == 2) music_on = 0;
				}

			if (state.x <= 241 && state.x >= 168 && state.y <= 562 && state.y >= 482)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (sounds_on == 2) sounds_on = 0;
				}

			if (music_on == 0 && sounds_on == 0)
			{
				al_draw_bitmap(screen_options, 0, 0, 0);
				al_draw_bitmap(button_exit, 0, 0, 0);
			}

			if (music_on == 1)
			{
				al_draw_bitmap(button_music, 0, 0, 0);
			}

			if (sounds_on == 1)
			{
				al_draw_bitmap(button_sounds, 0, 0, 0);
			}

			/// Animation and Coursor Options
			if (animation_on == 1 && state.x <= 241 && state.x >= 168 && state.y <= 476 + 8 + 160 && state.y >= 396 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) animation_on = 2;
			if (animation_on == 0 && state.x <= 241 && state.x >= 168 && state.y <= 476 + 8 + 160 && state.y >= 396 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) animation_on = 1;

			if (interface_on == 1 && state.x <= 241 && state.x >= 168 && state.y <= 562 + 8 + 160 && state.y >= 482 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) interface_on = 2;
			if (interface_on == 0 && state.x <= 241 && state.x >= 168 && state.y <= 562 + 8 + 160 && state.y >= 482 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) interface_on = 1;


			if (state.x <= 241 && state.x >= 168 && state.y <= 476 + 8 + 160 && state.y >= 396 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (animation_on == 2) animation_on = 0;
				}

			if (state.x <= 241 && state.x >= 168 && state.y <= 562 + 8 + 160 && state.y >= 482 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (interface_on == 2) interface_on = 0;
				}

			if (animation_on == 1)
			{
				al_draw_bitmap(button_music, 0, 0 + 8 + 160, 0);
				al_draw_textf(couree_24, color_white, 0.5 * SCREEN_W, SCREEN_H - 200 - 50, ALLEGRO_ALIGN_CENTRE, "WARNING!");
				al_draw_textf(couree_16, color_white, 0.5 * SCREEN_W, SCREEN_H - 175 - 50, ALLEGRO_ALIGN_CENTRE, "This animation has potential to induce SEIZURES ");
				al_draw_textf(couree_16, color_white, 0.5 * SCREEN_W, SCREEN_H - 150 - 50, ALLEGRO_ALIGN_CENTRE, "for people with Photosensitive EPILEPSY!");
				al_draw_textf(couree_24, color_white, 0.5 * SCREEN_W, SCREEN_H - 100 - 50, ALLEGRO_ALIGN_CENTRE, "OSTRZEZENIE!");
				al_draw_textf(couree_16, color_white, 0.5 * SCREEN_W, SCREEN_H - 75 - 50, ALLEGRO_ALIGN_CENTRE, "Ta animacja moze powodowac ATAK PADACZKI u osob chorych");
				al_draw_textf(couree_16, color_white, 0.5 * SCREEN_W, SCREEN_H - 50 - 50, ALLEGRO_ALIGN_CENTRE, "na EPILEPSJE o podlozu wizualnym!");
				//al_draw_textf(couree_16, color_gray, SCREEN_W / 2, SCREEN_H - 75, ALLEGRO_ALIGN_CENTRE, "Entry Animation may also slow down your computer!");
			}

			if (interface_on == 1)
			{
				al_draw_bitmap(button_sounds, 0, 0 + 8 + 160, 0);
			}

			/// HUD Options
			if (stats_hud_on == 1 && state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 476 && state.y >= 396)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) stats_hud_on = 2;
			if (stats_hud_on == 0 && state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 476 && state.y >= 396)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) stats_hud_on = 1;

			if (app_hud_on == 1 && state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 562 && state.y >= 482)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) app_hud_on = 2;
			if (app_hud_on == 0 && state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 562 && state.y >= 482)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) app_hud_on = 1;


			if (state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 476 && state.y >= 396)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (stats_hud_on == 2) stats_hud_on = 0;
				}

			if (state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 562 && state.y >= 482)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (app_hud_on == 2) app_hud_on = 0;
				}

			if (stats_hud_on == 1)
			{
				al_draw_bitmap(button_music, 0 + 385, 0, 0);
			}

			if (app_hud_on == 1)
			{
				al_draw_bitmap(button_sounds, 0 + 385, 0, 0);
			}

			/// HUD Enemy and Interface Options
			if (game_hud_on == 1 && state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 476 + 8 + 160 && state.y >= 396 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) game_hud_on = 2;
			if (game_hud_on == 0 && state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 476 + 8 + 160 && state.y >= 396 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) game_hud_on = 1;

			if (enemy_hud_on == 1 && state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 562 + 8 + 160 && state.y >= 482 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) enemy_hud_on = 2;
			if (enemy_hud_on == 0 && state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 562 + 8 + 160 && state.y >= 482 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) enemy_hud_on = 1;


			if (state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 476 + 8 + 160 && state.y >= 396 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (game_hud_on == 2) game_hud_on = 0;
				}

			if (state.x <= 241 + 385 && state.x >= 168 + 385 && state.y <= 562 + 8 + 160 && state.y >= 482 + 8 + 160)
				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					if (enemy_hud_on == 2) enemy_hud_on = 0;
				}

			if (game_hud_on == 1)
			{
				al_draw_bitmap(button_music, 0 + 385, 0 + 8 + 160, 0);
			}

			if (enemy_hud_on == 1)
			{
				al_draw_bitmap(button_sounds, 0 + 385, 0 + 8 + 160, 0);
			}

		}

		/// Credits Screen On
		if (credits_screen_on == 1)
		{
			menu_screen_on = 0;
			game_screen_on = 0;
			options_screen_on = 0;
			tutorial_screen_on = 0;
			victory_screen_on = 0;
			defeat_screen_on = 0;
			al_draw_bitmap(screen_credits, 0, 0, 0);
			al_draw_bitmap(button_exit, 0, 0, 0);
			al_draw_bitmap(button_exit, 0, 0, 0);
			//al_draw_multiline_text(couree_24, color_white, 200, 300, 500, 500, 0, "credits");

		}

		/// Exit to Menu Screen - Keyboard
		if (al_key_down(&keyboard, ALLEGRO_KEY_ESCAPE) && menu_screen_on == 0)
		{
			//al_install_mouse();
			if (game_screen_on == 1 || victory_screen_on == 1 || defeat_screen_on == 1) al_stop_samples();
			if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			if (tutorial_screen_on == 0 && options_screen_on == 0 && credits_screen_on == 0 && music_on == 1) al_play_sample(music_intro, music_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
			game_screen_on = 0;
			tutorial_screen_on = 0;
			options_screen_on = 0;
			credits_screen_on = 0;
			victory_screen_on = 0;
			defeat_screen_on = 0;
			frameCount = 0;
			shoot_arrow = 0;
			shoot_heal = 0;
			shoot_ultimate = 0;
			zombie_alive = 0;
			zombie2_alive = 0;
			zombie3_alive = 0;
			zombie4_alive = 0;
			human_alive = 0;
			human2_alive = 0;
			human3_alive = 0;
			human4_alive = 0;
			boss_alive = 0;
			reset_enemy_position();
			
		}

		/// Exit to Menu Screen - Mouse
		if (tutorial_screen_on == 1 || options_screen_on == 1 || credits_screen_on == 1 || victory_screen_on == 1 || defeat_screen_on == 1)
		{
			if (state.x <= 1256 && state.x >= 1234 && state.y <= 43 && state.y >= 21)
			{
				al_draw_bitmap(button_exit, 0, 0, 0);
				al_draw_bitmap(button_exit, 0, 0, 0);
				al_draw_bitmap(button_exit, 0, 0, 0);
				al_draw_bitmap(button_exit, 0, 0, 0);
				al_draw_bitmap(button_exit, 0, 0, 0);
				al_draw_bitmap(button_exit, 0, 0, 0);

				if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
				{
					if (victory_screen_on == 1 || defeat_screen_on == 1) al_stop_samples();
					if (music_on == 1 && victory_screen_on == 1 || music_on == 1 && defeat_screen_on == 1) al_play_sample(music_intro, music_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
					if (sounds_on == 1) al_play_sample(sound_click, sound_volume, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					tutorial_screen_on = 0;
					options_screen_on = 0;
					credits_screen_on = 0;
					victory_screen_on = 0;
					defeat_screen_on = 0;
					frameCount = 0;
					
				}
			}
		}

		/// Exit the Game
		if (al_key_down(&keyboard, ALLEGRO_KEY_ESCAPE) && menu_screen_on == 1 && entry_screen == 0 || al_key_down(&keyboard, ALLEGRO_KEY_ESCAPE) && entry_screen_on == 1)
		{
			loop = 1;
			al_uninstall_keyboard();
			al_uninstall_mouse();
		}

		/// Animation
		if (game_screen_on == 0 && entry_screen_on == 0 && animation_on == 1)
		{
			menu_time = 1.66666666666*frameCount / 100;
			al_draw_bitmap(smoke, -30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, -30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 2 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 2 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 3 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 3 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 4 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 4 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 5 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 5 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 6 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 6 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 7 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 7 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 8 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 8 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 9 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 9 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 10 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 10 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 11 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 11 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 12 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 12 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 13 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 13 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 14 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 14 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 15 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 15 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 16 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 16 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 17 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 17 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 18 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 18 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 19 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 19 * 2276 - 30 * menu_time, 0, 1);
			al_draw_bitmap(smoke, 20 * 2276 - 30 * menu_time, 0, 0);
			al_draw_bitmap(smoke, 20 * 2276 - 30 * menu_time, 0, 1);

			if (animation_on == 1)
				if (menu_time >= 10 && menu_time <= 10.09 || menu_time >= 10.25 && menu_time <= 10.34 || menu_time >= 20 && menu_time <= 20.09 || menu_time >= 20.25 && menu_time <= 20.34 || menu_time >= 30 && menu_time <= 30.09 || menu_time >= 30.25 && menu_time <= 30.34 || menu_time >= 40 && menu_time <= 40.09 || menu_time >= 40.25 && menu_time <= 40.34 || menu_time >= 50 && menu_time <= 50.09 || menu_time >= 50.25 && menu_time <= 50.34 || menu_time >= 60 && menu_time <= 60.09 || menu_time >= 60.25 && menu_time <= 60.34)
				{
					al_clear_to_color(color_white);
					al_draw_bitmap(screen_menu_background_white, 0, 0, 0);
				}

		}
		if (game_screen_on == 0 && entry_screen_on == 0 && animation_on == 0)
		{
			al_draw_bitmap(smoke, -30, 0, 0);
			al_draw_bitmap(smoke, -30, 0, 1);
		}

		/// App HUD
		if (app_hud_on == 1)
		{
			al_draw_textf(couree_16, color_white, 15, 10, 0, "Level: %i", game_level);
			al_draw_textf(couree_16, color_white, 15, 10 + 15, 0, "Frame: %i", currentFrame);
			al_draw_textf(couree_16, color_white, 15, 25 + 15, 0, "Count: %i", frameCount);
			al_draw_textf(couree_16, color_white, 15, SCREEN_H - 30, 0, "20M812-V%i.%i.%i %i.%i.%i by Adam Drabik", app_version, app_build, app_update, date_day, date_month, date_year);
			al_draw_textf(couree_16, color_white, 15, 40 + 15, 0, "Game Time: %.3f", game_time);
			al_draw_textf(couree_16, color_white, 15, 55 + 15, 0, "Menu Time: %.3f", menu_time);
			al_draw_textf(couree_16, color_white, 15, 70 + 15, 0, "App  Time: %.3f", app_time);
			al_draw_textf(couree_16, color_white, 15, 85 + 15, 0, "Screen Entry: %i", entry_screen_on);
			al_draw_textf(couree_16, color_white, 15, 100 + 15, 0, "Screen Menu: %i", menu_screen_on);
			al_draw_textf(couree_16, color_white, 15, 115 + 15, 0, "Screen Game: %i", game_screen_on);
			al_draw_textf(couree_16, color_white, 15, 130 + 15, 0, "Screen Tutorial: %i", tutorial_screen_on);
			al_draw_textf(couree_16, color_white, 15, 145 + 15, 0, "Screen Options: %i", options_screen_on);
			al_draw_textf(couree_16, color_white, 15, 160 + 15, 0, "Screen Credits: %i", credits_screen_on);
			al_draw_textf(couree_16, color_white, 15, 160 + 30, 0, "Screen Victory: %i", victory_screen_on);
			al_draw_textf(couree_16, color_white, 15, 160 + 45, 0, "Screen Defeat: %i", defeat_screen_on);
			al_draw_textf(couree_16, color_white, 15, 175 + 45, 0, "Audio Music: %i", music_on);
			al_draw_textf(couree_16, color_white, 15, 190 + 45, 0, "Audio Sounds: %i", sounds_on);
			al_draw_textf(couree_16, color_white, 15, 205 + 45, 0, "Entry Animation: %i", animation_on);
			al_draw_textf(couree_16, color_white, 15, 220 + 45, 0, "HUD App: %i", app_hud_on);
			al_draw_textf(couree_16, color_white, 15, 235 + 45, 0, "HUD Game: %i", game_hud_on);
			al_draw_textf(couree_16, color_white, 15, 250 + 45, 0, "HUD Enemy: %i", enemy_hud_on);
			al_draw_textf(couree_16, color_white, 15, 265 + 45, 0, "HUD Stats: %i", stats_hud_on);
			al_draw_textf(couree_16, color_white, 15, 280 + 45, 0, "Interface: %i", interface_on);
			al_draw_textf(couree_16, color_white, 15, 295 + 45, 0, "Density Zombie: %i", zombie_density);
			al_draw_textf(couree_16, color_white, 15, 310 + 45, 0, "Density Human:  %i", human_density);
			al_draw_textf(couree_16, color_white, 15, 325 + 45, 0, "Density Boss:  %i", boss_density);
			al_draw_textf(couree_16, color_white, 15, 340 + 45, 0, "Music Volume: %.2f", music_volume);
			al_draw_textf(couree_16, color_white, 15, 355 + 45, 0, "Sound Volume: %.2f", sound_volume);
			al_draw_textf(couree_16, color_white, 15, 370 + 45, 0, "X: %i Y: %i", state.x, state.y);

		}

		/// Stats HUD
		if (stats_hud_on == 1)
		{
			al_draw_textf(couree_16, color_white, 15 + 0, 10 + 500, 0, "LEVEL: %i", game_level);
			al_draw_textf(couree_16, color_gray, 15 + 0, 25 + 500, 0, "NORMAL HERO");
			al_draw_textf(couree_16, color_white, 15 + 0, 40 + 500, 0, "health: %.1f", nh_hp);
			al_draw_textf(couree_16, color_white, 15 + 0, 55 + 500, 0, "damage: %.1f", nh_dmg);
			al_draw_textf(couree_16, color_white, 15 + 0, 70 + 500, 0, "speed:  %.1f", nh_spd);
			al_draw_textf(couree_16, color_white, 15 + 0, 85 + 500, 0, "movement: %.1f", nh_mvn);
			al_draw_textf(couree_16, color_white, 15 + 0, 100 + 500, 0, "morality: %.1f", morality);

			al_draw_textf(couree_16, color_light, 15 + 0, 115 + 500, 0, "LIGHT HERO");
			al_draw_textf(couree_16, color_white, 15 + 0, 130 + 500, 0, "health: %.1f", lh_hp);
			al_draw_textf(couree_16, color_white, 15 + 0, 145 + 500, 0, "damage: %.1f", lh_dmg);
			al_draw_textf(couree_16, color_white, 15 + 0, 160 + 500, 0, "speed:  %.1f", lh_spd);
			al_draw_textf(couree_16, color_white, 15 + 0, 175 + 500, 0, "cooldown: %.1f", lh_cd);
			al_draw_textf(couree_16, color_white, 15 + 0, 190 + 500, 0, "movement: %.1f", lh_mvn);

			al_draw_textf(couree_16, color_dark, 15 + 0, 205 + 500, 0, "DARK HERO");
			al_draw_textf(couree_16, color_white, 15 + 0, 220 + 500, 0, "health: %.1f", dh_hp);
			al_draw_textf(couree_16, color_white, 15 + 0, 235 + 500, 0, "damage: %.1f", dh_dmg);
			al_draw_textf(couree_16, color_white, 15 + 0, 250 + 500, 0, "speed:  %.1f", dh_spd);
			al_draw_textf(couree_16, color_white, 15 + 0, 265 + 500, 0, "cooldown: %.1f", dh_cd);
			al_draw_textf(couree_16, color_white, 15 + 0, 280 + 500, 0, "movement: %.1f", dh_mvn);

			al_draw_textf(couree_16, color_zombie, 15 + 0, 295 + 500, 0, "ZOMBIE ENEMY");
			al_draw_textf(couree_16, color_white, 15 + 0, 310 + 500, 0, "health: %.1f", ze_hp);
			al_draw_textf(couree_16, color_white, 15 + 0, 325 + 500, 0, "damage: %.1f", ze_dmg);
			al_draw_textf(couree_16, color_white, 15 + 0, 340 + 500, 0, "movement: %.1f", ze_mvn);

			al_draw_textf(couree_16, color_human, 15 + 0, 355 + 500, 0, "HUMAN ENEMY");
			al_draw_textf(couree_16, color_white, 15 + 0, 370 + 500, 0, "health: %.1f", he_hp);
			al_draw_textf(couree_16, color_white, 15 + 0, 385 + 500, 0, "damage: %.1f", he_dmg);
			al_draw_textf(couree_16, color_white, 15 + 0, 400 + 500, 0, "movement: %.1f", he_mvn);

			al_draw_textf(couree_16, color_red, 15 + 0, 415 + 500, 0, "BOSS");
			al_draw_textf(couree_16, color_white, 15 + 0, 430 + 500, 0, "health: %.1f", be_hp);
			al_draw_textf(couree_16, color_white, 15 + 0, 445 + 500, 0, "damage: %.1f", be_dmg);
			al_draw_textf(couree_16, color_white, 15 + 0, 460 + 500, 0, "movement: %.1f", be_mvn);
			al_draw_textf(couree_16, color_white, 15 + 0, 475 + 500, 0, "speed: %.1f", be_spd);
			al_draw_textf(couree_16, color_white, 15 + 0, 490 + 500, 0, "spawn time: %.0f", boss_time);

			
		}

		/// Game HUD
		if (game_hud_on == 1)
		{
			al_draw_textf(couree_16, color_white, 15 + 1100, 10, 0, "LEVEL: %i", game_level);

			if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				al_draw_textf(couree_16, color_gray, 15 + 1100, 25, 0, "NORMAL HERO");
			if (light_points_current >= light_points_max)
				al_draw_textf(couree_16, color_light, 15 + 1100, 25, 0, "LIGHT HERO");
			if (dark_points_current >= dark_points_max)
				al_draw_textf(couree_16, color_dark, 15 + 1100, 25, 0, "DARK HERO");

			al_draw_textf(couree_16, color_white, 15 + 1100, 40, 0, "X: %.0f Y: %.0f", hero_x, hero_y);

			if (light_points_current < light_points_max && dark_points_current < dark_points_max)
				al_draw_textf(couree_16, color_white, 15 + 1100, 55, 0, "HP: %.0f/%.0f", hero_current_hp, nh_hp);
			if (light_points_current >= light_points_max)
				al_draw_textf(couree_16, color_white, 15 + 1100, 55, 0, "HP: %.0f/%.0f", hero_current_hp, lh_hp);
			if (dark_points_current >= dark_points_max)
				al_draw_textf(couree_16, color_white, 15 + 1100, 55, 0, "HP: %.0f/%.0f", hero_current_hp, dh_hp);

			al_draw_textf(couree_16, color_white, 15 + 1100, 70, 0, "LIGHT: %.0f/%.0f", light_points_current, light_points_max);
			al_draw_textf(couree_16, color_white, 15 + 1100, 85, 0, "DARK:  %.0f/%.0f", dark_points_current, dark_points_max);
			al_draw_textf(couree_16, color_white, 15 + 1100, 100, 0, "[J]:%i | %.0f:%.0f", shoot_arrow, arrow_x, arrow_y);
			al_draw_textf(couree_16, color_white, 15 + 1100, 115, 0, "[K]:%i | %.0f:%.0f", shoot_heal, heal_x, heal_y);
			al_draw_textf(couree_16, color_white, 15 + 1100, 130, 0, "[L]:%i | %.0f:%.0f", shoot_ultimate, ultimate_x, ultimate_y);
		}

		/// Enemy HUD 
		if (enemy_hud_on == 1)
		{	
			
			if (zombie_density >= 1)
			{
				al_draw_textf(couree_16, color_zombie, 15 + 1100, 115 + 45, 0, "ZOMBIE 1");
				al_draw_textf(couree_16, color_white, 15 + 1100, 130 + 45, 0, "Alive: %i", zombie_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 145 + 45, 0, "HP: %.0f/%.0f", zombie_current_hp, ze_hp);
				al_draw_textf(couree_16, color_white, 15 + 1100, 160 + 45, 0, "X: %.0f Y: %.0f", zombie_x, zombie_y);
				al_draw_textf(couree_16, color_white, 15 + 1100, 175 + 45, 0, "Rand: %.0f x %.0f", random_z1x, random_z1y);
			}

			if (zombie_density >= 2)
			{
				al_draw_textf(couree_16, color_zombie, 15 + 1100, 205 + 45, 0, "ZOMBIE 2");
				al_draw_textf(couree_16, color_white, 15 + 1100, 220 + 45, 0, "Alive: %i", zombie2_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 235 + 45, 0, "HP: %.0f/%.0f", zombie2_current_hp, ze_hp);
				al_draw_textf(couree_16, color_white, 15 + 1100, 250 + 45, 0, "X: %.0f Y: %.0f", zombie2_x, zombie2_y);
				al_draw_textf(couree_16, color_white, 15 + 1100, 265 + 45, 0, "Rand: %.0f x %.0f", random_z2x, random_z2y);
			}

			if (zombie_density >= 3)
			{
				al_draw_textf(couree_16, color_zombie, 15 + 1100, 295 + 45, 0, "ZOMBIE 3");
				al_draw_textf(couree_16, color_white, 15 + 1100, 310 + 45, 0, "Alive: %i", zombie3_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 325 + 45, 0, "HP: %.0f/%.0f", zombie3_current_hp, ze_hp);
				al_draw_textf(couree_16, color_white, 15 + 1100, 340 + 45, 0, "X: %.0f Y: %.0f", zombie3_x, zombie3_y);
				al_draw_textf(couree_16, color_white, 15 + 1100, 355 + 45, 0, "Rand: %.0f x %.0f", random_z3x, random_z3y);
			}

			if (zombie_density >= 4)
			{
				al_draw_textf(couree_16, color_zombie, 15 + 1100, 295 + 45 + 90, 0, "ZOMBIE 4");
				al_draw_textf(couree_16, color_white, 15 + 1100, 310 + 45 + 90, 0, "Alive: %i", zombie4_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 325 + 45 + 90, 0, "HP: %.0f/%.0f", zombie4_current_hp, ze_hp);
				al_draw_textf(couree_16, color_white, 15 + 1100, 340 + 45 + 90, 0, "X: %.0f Y: %.0f", zombie4_x, zombie4_y);
				al_draw_textf(couree_16, color_white, 15 + 1100, 355 + 45 + 90, 0, "Rand: %.0f x %.0f", random_z4x, random_z4y);
			}

			if (human_density >= 1)
			{
				al_draw_textf(couree_16, color_human, 15 + 1100, 385 + 45 + 90, 0, "HUMAN 1");
				al_draw_textf(couree_16, color_white, 15 + 1100, 400 + 45 + 90, 0, "Alive: %i", human_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 415 + 45 + 90, 0, "HP: %.0f/%.0f", human_current_hp, he_hp);
				al_draw_textf(couree_16, color_white, 15 + 1100, 430 + 45 + 90, 0, "X: %.0f Y: %.0f", human_x, human_y);
				al_draw_textf(couree_16, color_white, 15 + 1100, 445 + 45 + 90, 0, "Rand: %.0f x %.0f", random_h1x, random_h1y);
			}

			if (human_density >= 2)
			{
				al_draw_textf(couree_16, color_human, 15 + 1100, 475 + 45 + 90, 0, "HUMAN 2");
				al_draw_textf(couree_16, color_white, 15 + 1100, 490 + 45 + 90, 0, "Alive: %i", human2_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 505 + 45 + 90, 0, "HP: %.0f/%.0f", human2_current_hp, he_hp);
				al_draw_textf(couree_16, color_white, 15 + 1100, 520 + 45 + 90, 0, "X: %.0f Y: %.0f", human2_x, human2_y);
				al_draw_textf(couree_16, color_white, 15 + 1100, 535 + 45 + 90, 0, "Rand: %.0f x %.0f", random_h2x, random_h2y);
			}

			if (human_density >= 3)
			{
				al_draw_textf(couree_16, color_human, 15 + 1100, 565 + 45 + 90, 0, "HUMAN 3");
				al_draw_textf(couree_16, color_white, 15 + 1100, 580 + 45 + 90, 0, "Alive: %i", human3_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 595 + 45 + 90, 0, "HP: %.0f/%.0f", human3_current_hp, he_hp);
				al_draw_textf(couree_16, color_white, 15 + 1100, 610 + 45 + 90, 0, "X: %.0f Y: %.0f", human3_x, human3_y);
				al_draw_textf(couree_16, color_white, 15 + 1100, 625 + 45 + 90, 0, "Rand: %.0f x %.0f", random_h3x, random_h3y);
			}

			if (human_density >= 4)
			{
				al_draw_textf(couree_16, color_human, 15 + 1100, 655 + 45 + 90, 0, "HUMAN 4");
				al_draw_textf(couree_16, color_white, 15 + 1100, 670 + 45 + 90, 0, "Alive: %i", human4_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 685 + 45 + 90, 0, "HP: %.0f/%.0f", human4_current_hp, he_hp);
				al_draw_textf(couree_16, color_white, 15 + 1100, 700 + 45 + 90, 0, "X: %.0f Y: %.0f", human4_x, human4_y);
				al_draw_textf(couree_16, color_white, 15 + 1100, 715 + 45 + 90, 0, "Rand: %.0f x %.0f", random_h4x, random_h4y);
			}

			if (boss_density >= 1)
			{
				al_draw_textf(couree_16, color_red, 15 + 1100, 745 + 45 + 90, 0, "BOSS");
				al_draw_textf(couree_16, color_white, 15 + 1100, 760 + 45 + 90, 0, "Alive: %i", boss_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 775 + 45 + 90, 0, "HP: %.0f/%.0f", boss_current_hp, be_hp);
				al_draw_textf(couree_16, color_white, 15 + 1100, 790 + 45 + 90, 0, "X: %.0f Y: %.0f", boss_x, boss_y);
				al_draw_textf(couree_16, color_white, 15 + 1100, 805 + 45 + 90, 0, "Fireball: %i", fireball_alive);
				al_draw_textf(couree_16, color_white, 15 + 1100, 820 + 45 + 90, 0, "X: %.0f Y: %.0f", fireball_x, fireball_y);

			}
		}
		
		/// Time Count
		app_time = al_get_time();
		
		/// Display Draw
		al_flip_display();
	}

	/// Destroy 
	if (app == 1)
	{
		al_destroy_bitmap(screen_menu_background_black);
		al_destroy_bitmap(screen_menu_background_white);
		al_destroy_bitmap(screen_menu_newgame_black);
		al_destroy_bitmap(screen_menu_continue_black);
		al_destroy_bitmap(screen_menu_tutorial_black);
		al_destroy_bitmap(screen_menu_options_black);
		al_destroy_bitmap(screen_menu_credits_black);
		al_destroy_bitmap(screen_menu_exit_black);

		al_destroy_bitmap(screen_tutorial);
		al_destroy_bitmap(screen_credits);
		al_destroy_bitmap(screen_options);
		al_destroy_bitmap(screen_victory);
		al_destroy_bitmap(screen_defeat);
		al_destroy_bitmap(screen_score_continue);

		al_destroy_bitmap(button_exit);
		al_destroy_bitmap(button_music);
		al_destroy_bitmap(button_sounds);

		al_destroy_bitmap(layer0);
		al_destroy_bitmap(layer1);
		al_destroy_bitmap(layer2);
		al_destroy_bitmap(layer3);
		al_destroy_bitmap(layer4);
		al_destroy_bitmap(layer5);

		al_destroy_bitmap(layer0_green);
		al_destroy_bitmap(layer1_green);
		al_destroy_bitmap(layer2_green);
		al_destroy_bitmap(layer3_green);
		al_destroy_bitmap(layer4_green);
		al_destroy_bitmap(layer5_green);

		al_destroy_bitmap(layer0_blue);
		al_destroy_bitmap(layer1_blue);
		al_destroy_bitmap(layer2_blue);
		al_destroy_bitmap(layer3_blue);
		al_destroy_bitmap(layer4_blue);
		al_destroy_bitmap(layer5_blue);

		al_destroy_bitmap(layer0_red);
		al_destroy_bitmap(layer1_red);
		al_destroy_bitmap(layer2_red);
		al_destroy_bitmap(layer3_red);
		al_destroy_bitmap(layer4_red);
		al_destroy_bitmap(layer5_red);

		al_destroy_bitmap(layer0_orange);
		al_destroy_bitmap(layer1_orange);
		al_destroy_bitmap(layer2_orange);
		al_destroy_bitmap(layer3_orange);
		al_destroy_bitmap(layer4_orange);
		al_destroy_bitmap(layer5_orange);

		al_destroy_bitmap(moon_normal);
		al_destroy_bitmap(moon_blood);
		al_destroy_bitmap(sky_stars_blue);
		al_destroy_bitmap(sky_stars_green);
		al_destroy_bitmap(sky_stars_normal);
		al_destroy_bitmap(sky_stars_normal_2);

		al_destroy_bitmap(hero_noblood_1);
		al_destroy_bitmap(hero_noblood_2);
		al_destroy_bitmap(hero_blood_1);
		al_destroy_bitmap(hero_blood_2);
		al_destroy_bitmap(zombie_unhealed_1);
		al_destroy_bitmap(zombie_unhealed_2);
		al_destroy_bitmap(zombie_healed_1);
		al_destroy_bitmap(zombie_healed_2);
		al_destroy_bitmap(human_unhealed_1);
		al_destroy_bitmap(human_unhealed_2);
		al_destroy_bitmap(human_healed_1);
		al_destroy_bitmap(human_healed_2);
		al_destroy_bitmap(light_1);
		al_destroy_bitmap(light_2);
		al_destroy_bitmap(light_3);
		al_destroy_bitmap(light_4);
		al_destroy_bitmap(light_5);
		al_destroy_bitmap(dark_1);
		al_destroy_bitmap(dark_2);
		al_destroy_bitmap(dark_3);
		al_destroy_bitmap(dark_4);
		al_destroy_bitmap(dark_5);
		al_destroy_bitmap(boss_1);
		al_destroy_bitmap(boss_2);

		al_destroy_bitmap(zombie_collision_damaged);
		al_destroy_bitmap(zombie_collision_healed);
		al_destroy_bitmap(human_collision_damaged);
		al_destroy_bitmap(human_collision_healed);
		al_destroy_bitmap(boss_collision_damaged);
		al_destroy_bitmap(boss_collision_healed);
		al_destroy_bitmap(hero_collision_damaged);
		al_destroy_bitmap(superhero_collision_damaged);

		al_destroy_bitmap(normal_attack);
		al_destroy_bitmap(normal_heal);
		al_destroy_bitmap(light_attack);
		al_destroy_bitmap(light_heal);
		al_destroy_bitmap(light_ultimate);
		al_destroy_bitmap(dark_attack);
		al_destroy_bitmap(dark_heal);
		al_destroy_bitmap(dark_ultimate);
		al_destroy_bitmap(boss_attack);

		al_destroy_bitmap(smoke);
		al_destroy_bitmap(logo);
		al_destroy_bitmap(screen_black);

		al_destroy_bitmap(interface_background);
		al_destroy_bitmap(interface_frame);
		al_destroy_bitmap(interface_j_normal);
		al_destroy_bitmap(interface_k_normal);
		al_destroy_bitmap(interface_l_normal);
		al_destroy_bitmap(interface_j_light);
		al_destroy_bitmap(interface_k_light);
		al_destroy_bitmap(interface_l_light);
		al_destroy_bitmap(interface_j_dark);
		al_destroy_bitmap(interface_k_dark);
		al_destroy_bitmap(interface_l_dark);

		al_destroy_sample(music_intro);
		al_destroy_sample(music_main);
		al_destroy_sample(sound_click);
		al_destroy_sample(sound_heal);
		al_destroy_sample(sound_hero_hit);
		al_destroy_sample(sound_human_hit);
		al_destroy_sample(sound_zombie_hit);
		al_destroy_sample(sound_light_j);
		al_destroy_sample(sound_light_k);
		al_destroy_sample(sound_shoot);
		al_destroy_sample(sound_step);
		al_destroy_sample(sound_ultimate);

		al_destroy_font(font8);
		al_destroy_font(couree);
		al_destroy_font(couree_8);
		al_destroy_font(couree_12);
		al_destroy_font(couree_16);
		al_destroy_font(couree_24);
		al_destroy_timer(timer);
		al_destroy_display(display);
		al_destroy_event_queue(event_queue);
	}

	/// Saving to txt
	save_stats();
	save_options();
	save_level();
	save_spawn();
	save_hero();

	return 0;
}



