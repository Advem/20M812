Projekt:  20M812
Autor: 	Adam Drabik
Aktualizacja: V1.#

#1.X - grafiki elementów w formacie .png

#2.1 - instalacja i inicjalizacja bibliotek, struktur, funkcji
#2.2 - dodano okno aplikacji o rozdzielczości 1280x1024
#2.3 - dodano animacje "entry_screen"
#2.4 - wyswietlanie tekst kodu na ekraniu tzw."app HUD"
#2.5 - wyswietlanie menu gry oraz przyciskow i ich animacji "menu_screen"
#2.6 - wyswietlanie ekranów menu - aktywacja przycisków
#2.7 - naprawiono bledy wczytywania plikow png (animacja "entry screen" - wylaczona)
#2.8 - dodano ekrany: "tutorial", "options" oraz przyciski wylaczania ekranow oraz 
		zmiany opcji audio
#2.9 - dodano przyciski wlaczania i wylaczania audio oraz hud; dodano "app HUD" i "game HUD"
#2.10 - dodano odczytywanie i zapisywanie plikow tekstowych stats.txt options.txt oraz level.txt
	dodano zapisywanie ustawien aplikacji do pliku
	dodano wczytywanie statystyk postaci z pliku
	dodano zapisywanie poziomu planszy do pliku
	dodano uruchomienie poziomu planszy (przycisk continue) z pliku tekstowego level.txt
#2.11 - dodano ekran "credits"
#2.12 - naprawiono blad animacji "entry screen" - animacja wyswietla 140 klatek
#2.13 - dodano wiecej przyciskow do ekranu options (entry animation, interface, enemy HUD, cursor)
#2.14 - dodano opcje wylaczenia animacji "entry screen" (entry animation_on = 0;), 
		ktora mozna uruchamiac z ekranu opcji w aplikacji
#2.15 - dodano kolejne bitmapy plansz dla poszczegolnych poziomow
	dodano animacje ksiezyca i blasku ksiezyca dla poziomow
	naprawiono wyglad notyfikacji w konsoli
	dodano podswietlenie przycisku X w prawym gornym rogu w momencie interakcji z kursorem
	dodano ostrzezenie w momencie wlaczenia animacji entry screen w opcjach
		!pojawil sie ponownie blad wczytywania bitmap gdy animation=1;!
#2.16 - zmniejszenie ilosci klate animacji entry screen z 140 do 21 tak aby uniknac bledu
	dodanie animacji nieba i gwiazd
	dodanie zmiennej czasu, ktora resetuje sie wraz z pojawieniem planszy rozgrywki
		(posluzy ona do manipulacji ruchu postaciami)
	dodanie opcji zmiany poziomu bezposredni z menu gry

#3.0 - dodano grafike bohatera wraz z animacja
	dodano grafike zombie wraz z animacja
	zaimplementowano ruch zombie
	zaimplementowano mozliwosc poruszania sie bohaterem za pomoca klawiszy W,S,A,D 
		o predkosci odczytywanej z pliku stats.txt
#3.1 - zmieniono grafiki wszystkich ekranow
	dodano nowa animacje (mgla) do ekranu menu 
	dodano zmienna czasu istnienia menu
	zoptymalizowano wielkosc bitmap
#3.2 - dodano nowa animacje entry screen, ktora w porownaniu do poprzedniej
		sklada sie z 2 bitmap, a nie ze 140
	dodano animacje (burza) do ekranu menu, ktora uruchamia sie co 10 sekund
	dodano opcje wylaczenia nowej animacji entry screen oraz wylaczenia animacji burza
	dodano ostrzezenie w ekraniu options o animacji burza 
		(moze ona wywolac atak u ludzi chorych na epilepsje)
	usunieto z folderu z aplikacja pliki starej animacji entry screen
#3.3 - dodano grafiki pociskow
	dodano pozliwosc wystrzalu pocisku pod klawiszem J
	zaimplementowano ruch jednego pocisku
#3.4 - dodano ruch pociskow i zombie
	!bledy predkosci przy ruchu myszka!
#3.5 - dodano losowe pojawianie sie zombie na planszy oraz zmienianie jego wspolzednej y 
		wraz z ponownym uruchomieniem planszy
	naprawiono pojawianie sie zombie (ogarniczenia ekranu)
	dodano grafiki wraz z animacjami: uleczony zombie, zraniony bohater, 
		light hero, dark hero wraz z pociskami dla poszczegolnej klasy bohatera
	dodano zmienne aktualnego zdrowia postaci oraz punktow moralnosci
	zaktualizowano game HUD
#3.6 - dodano mozliwosc zmiany predkosci animacji postaci
	zaktualizowano game HUD
	dodano interfejs
	dodano pasek zdorwia oraz paski punktow morlanosci zaleznych od aktaulnych wartosci
	dodano postac czlowieka, jego animacje oraz ruch
	dodano ponowne pojawianie sie zombie oraz czlowieka
#3.7 - przystosowano grafike menu do aktaulnych klawiszy sterowania
	dodano grafiki przyciskow klawiszy J, K, L pojawiajace siew momencie, 
		gdy dana umiejetnosc jest dostepna
	naprawiono pasek zdrowia i punktow moralnosci tak, ze gdy dana wartosc aktualna jest 
		wieksza od maksymalnej pasek nie wychodzil po za obramowanie
	dodano automatyczne zmniejszanie aktualnego zdrowia, gdy bohater zmieniajac klase
		zmniejszal swoje maksymalne zdrowie
#3.8 - dodano nowy plik tekstowy "hero.txt" w ktorym sa zapisywane dane o aktualnym poziomie
		zdrowia bohatera, aktualnych puktach moralnosci oraz 			
		maksymalnych puktach - te ostatnie beda zmienialy sie wraz z poziomami
	dodano odczytywanie i zapisywanie pliku "hero.txt" 
#3.9 - dodano zmienna predkosci poruszania sie bohatera odczytywana z pliku stats.txt
		zaleznie od klasy bohatera
	dodano zmienna predkosci poruszania sie pocisku odczytywana z pliku stats.txt
		zaleznie od klasy bohatera
	dodano pozostale grafiki poziomow do planszy gry, zaleznie od aktualnego poziomu
	zaktualizowano interfejs, tak aby umiejetnosci bohatera normal_hero
		byly lepiej widoczne	
#3.10 - rozszerzono ilosc pojawianych zombie i human do 4	
	dodano zmienne losowe okreslajace wartosci zmiennej x oraz y dla przeciwnikow, 	
		tak aby ich pojawianie bylo losowe
	zaktualizowano game HUD
#3.11 - dodano postac potowra (boss), jego grafike, animacje, ruch statystyki odczytywane z 
		pliku stats.txt
	naprawiono warunki wyswietlania sie przeciwnikow tak, aby w momnecie, gdy ich 
		aktualne zdrowie jest mniejsze lub rowne zero, grafika postaci przestala
		sie wyswietlac na ekranie
	dodano zmienne okreslajace ilosc pojawianych sie przeciwnikow i bossa
	dodano nowy plik tekstowy "spawn.txt" w ktorym zapisywane sa informacje o ilosci
		przeciwnikow wyswietlanych na ekranie 
	dodano odczytywanie i zapisywanie pliku spawn.txt
	zaktualizowano game HUD tak aby wyswietlal informacje tylko o przeciwnikach, ktorych
		modele sa wyswietlane na ekranie
	zaktualizowano app HUD
	zaktualizowano stats HUD
#3.12 - dodano przycisk enemy HUD, ktorym mozna wylaczac wyswietlane informacje o aktualnie
		wyswietlanych statystykach przeciwnikow

#4.1 - dodano kolizje strzaly oraz apteczki z zombie1
	dodano system utraty i zdobywania punktow zdrowia przez zombie
	dodano system utraty zdrowia bohatera gdy nieuleczony zombie1 przejdzie przez ekran
	dodano grafike zombie w momencie okreslonej kolizji
	dodano funkcje zwiekszajaca predkosc ruchu zombie1 po zakonczeniu kolizji
	dodano system zdobywania i tracenia puktow moralnosci zaleznych od kolizji z zombie1
	zaktualizowano game HUD
	poprawiono wyglad kolizji dla normal hero
	poprawiono wyglad notyfikacji konsoli
#4.2 - dodano kolizje ultimate z zombie1 - ultimate light leczy wszystkie postacie w lini, a
		ultimate dark zadaje obrazenia wszystkim postaciom w linii
	dodano czas odnowienia ultimate zalezny od wartosci odczytywanej z pliku stats.txt
	dodano wspolczynnik moralnosci (morality) odczytywany z pliku stats.txt, ktory okresla
		ilosc przyznawanych punktow moralnosci podczas kolizji z przeciwnikiem
	dodano funkcje okreslajaca poczatkowe statystyki bohaterow ( wartosci odczytywane z
		pliku stats.txt beda resetowane, w momencie uruchomienia nowej gry)
	zmieniono funkcje void dla zombie2, zombie3, zombie4 tak aby przygotowac je na 
		implementacje kolejnych kolizji
	zaktualizowano game HUD, stats HUD
#4.3 - poprawiono grafike pojawiania sie uleczonego zombie w momencie kolizji
	poprawiono wyglad kolizji light oraz dark hero 
	dodano kolizje z zombie2, zombie3, zombie4
	poprawiono dzialanie ultimate - umiejetnosc odnawia sie co cooldown (stats.txt)
		oraz jego dzialanie trwa maksymalnie 3 sekundy od uruchomienia
	poprawiono dzialanie przydzielania punktow moralnosci - punkty te nie moga
		posiadac wartosci ujemnej
	dodano funkcje zabierajaca punkty moralnosci w momencje przeciwnej kolizji 
		zaleznej od poziomu gry (gdy light hero zabije czlowieka zostanie
		bardziej ukarany - zaleznie od poziomu)
	ogarniczono zasieg dzialania ultimate do maksymalnie 1300 jednostek
	!BUG 4.3.1: gdy bohater posiada obydwie klasy jego statystyki sa podwojone
		a kolizje wygrywa dark hero (wiecej obrazen)
	!BUG 4.3.2: ultimate czasami nie zadaje obrazen (zwlaszcza gdy jest uruchamiany
		w czasie, ktorego %3=0)
#4.4 - dodano kolizje z human, human2, human3, human4
	poprawiono animacje podczas kolizji
	dodano funkcje pojawiania bossa oraz znikania pozostalych przeciwnikow wraz z animacja
	dodano kolizje z bossem
	poprawino wszystkie kolizje
	dodano zmienna boss_time ktora okresla moment pojawienia sie boss zmienna z poziomami
#4.5 - dodano grafkie, animacje dla kuli ognia - atak bossa
	dodano mechanike strzelania bossa, tak aby pociski celowaly w bohatera
	dodano kolizje bohatera z kula ognia
	dodano grafike obrazen bossa
	dodano grafike obrazen bohatera dla wszystkich klas
#4.6 - dodano ekrany victory oraz defeat w momecnie przejscia poziomu lub porazki
	dodano system awansowania na wyzszy poziom planszy oraz system resetowania statystyk

#5.0 - dodano system zmieniania statystyk i przelicznikow przeciwnikow
#5.1 - zaktualizowano statystyki tak, aby z kolejnym poziomem gra byla trudniejsza
#5.2 - dodano system zmniejszajacy punkty moralnosci na poczatku nowego poziomu
	usunieto mozliwosc posiadania nieskonczonej ilosci punktow moralnosci
	zaktualizowano system statystyk przeciwnikow
	naprawiono blad: przeciwnicy na poczatku poziomu posiadali mniej zdrowia niz powinni 
	dodano maksymalne punkty moralnosci zalezne od poziomu
	naprawiono przyznawanie punktow zdrowia po zmienie klasy
	naprawiono bledy zwiazane z kontynuacja gry po zamknieciu aplikacji
	naprawiono blad: gracz po uleczeniu bossa nie wygrywal poziomu
	zwiekszono dlugosc trwania animacji smoke (z 3 powtorzen do 20)
	poprawiono victory oraz defeat screen
	poprawiono estetyke kodu
#5.3 - zaktualizowano tutorial screen
	zaktualizowano credits screen
	poprawiono smoke animation
	dodano animacje mgly do planszy rozgrywki
	
#6.0 - dodano addony audio i codecki audio
#6.1 - dodano muzyke intro i muzyke w grze
	dodano efekt dzwiekowy przy wcisnieciu przycisku w menu	
	dodano efekty dzwiekowe dla uzycia klawiszy J,K,L
	dodano efekty dzwiekowe dla kolizji zombie ze strzala oraz zombie z apteczka
	dodano efekty dzwiekowe dla kolizji czlowieka ze strzala oraz czlowieka z apteczka
	dodano efekty dzwiekowe dla kozizji bohatera
	naprawiono bledy zwiazane z odtwarzaniem dzwiekow i ich wylaczaniem
	poprawiono wyglad grafiki hero_blood2 oraz hero_noblood2
#6.2 - dodano na ekranie options mozliwosc zmiany glosnosci muzyki oraz efektami dzwiekowymi, 
	dodano zapisywanie ustawien glosnosci do pliku tekstowego
#6.3 - dodano pasek zdrowia bossa
#6.4 - naprawiono blad niewidzialnych kuli ognia
	poprawiono predkosc poruszania sie ksiezyca i gwiazd dla poziomow
	poprawiono zaleznosci glosnosci od ustawien
	dodano dzwiek w momencie gdy bohater tracil zdrowie przez zombie i ludzi
	dodano dzwieki do dodatkowych przyciskow
	zaktualizowano HUDy
	zaktualizowano options screen oraz credits screen
#6.5 - !!!naprawiono blad "sRGB", aplikacja teraz uruchamia sie odrazu!!!
#6.6 - !!!zniwelowano wplyw bledy, ktory przyspieszal czas w momencie poruszania myszka!!!
	!!!naprawiono blad "vcruntime140D.dll missing" oraz "ucrtbased.dll missing"!!!
	zaktualizowano credits screen
	naprawiono bledy w dzwiekach
