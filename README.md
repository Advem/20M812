**20M812**
(ZOMBIZ) to projekt gry wykonany na potrzeby zaliczenia przedmiotu uczelnianego (Proceduralne Języki Programowania 2) na 3.
semestrze (marzec 2018) kierunku Fizyka Techniczna (Informatyka Stosowana) wykonany przy użyciu języka C oraz biblioteki graficznej Allegro5.
Wszystkie grafiki zostały narysowane przez autora w stylu PixelArt za pomocą oprogramowania Adobe.

W folderze Documentation znajduje się plik 20M812-S3.pdf, który jest pełną dokumentacją programu (SRS - Software Requirements Specification)

**Streszczenie:**
Aplikacja pod tytułem “20M812” to gra zręcznościowa z gatunku strzelanek
(z ang. Shooter), w której wcielamy się w postać kusznika, który ma za zadanie
przetrwać atak zombie (nieumarłych). Na planszy pojawiać się będą 3 typy przeciwników, z
którymi bohater będzie mógł wchodzić w interakcje: zombie, ludzie,
potwory. Gracz w początkowej fazie gry ma do dyspozycji dwie umiejętności, z których
będzie korzystał: strzał z kuszy oraz wyrzucenie apteczki.
Gracz będzie mógł zabijać oraz leczyć apteczkami zarówno zombie, ludzi jak i potwory.
Gracz będzie nagradzany punktami moralność (punkty dobra i punkty zła), zależnie od
podejmowanych decyzji podczas rozgrywki. Zebranie poszczególnych punktów moralności
przez gracza powoduje, że bohater zamieni się w inną postać (postać dobra lub zła) i
odblokowuje inne umiejętności oraz superumiejętność. Gra została podzielona na 10
poziomów, na których końcu bohater będzie walczył z określonym potworem.
Przechodzenie na wyższy poziom ma miejsce w momencie, w którym bohater przetrwa całą fale przeciwników na danej planszy. 
Gracz przegra poziom w momencie, gdy straci całe swoje zdrowie. 
Utrata części zdrowia następuje wtedy, gdy otrzyma on obrażenia
od potworów, lub gdy przeciwnicy przejdą przez całą szerokość ekranu z brakującymi
punktami zdrowia (gracz zmuszony jest do zabijania lub leczenia przeciwników).
Wraz z przechodzeniem na kolejne poziomy bohater oraz wrogowie będą otrzymywali
dodatkowe bonusy do umiejętności i statystyk w taki sposób,
aby przejście kolejnych poziomów było coraz trudniejsze.
Gra zakończy się wtedy, kiedy bohater przetrwa ostatni 10 poziom.


**Opis programu:**
Cała aplikacja bazuje na pętli while, która wykonuje się 60 razy na sekundę.
Na ekranie wyświetlane są grafiki, a ich położenie określane jest za pomocą współrzędnych (zmienne X i Y).
Po uruchomieniu aplikacji wyświetlane jest intro - animacja obrazów, a następnie wyświetlane jest menu.
Wybór opcji menu głównego zmienia wartość zmiennej logicznej konkretnej podstrony, w momencie, gdy użytkownik wciśnie przycisk myszy, a
kursor myszy znajduje się w określonym miejscu na ekranie. Sprawdzany jest wówczas warunek, która podstrona jest aktywna. 
Wówczas na ekranie wyświetlane są inne grafiki i możliwe interakcje.

**Menu opcji:**
- ukrycie interfejsu graficznego
- ukrycie widoku deweloperskiego
- ukrycie statystyk przeciwników
- zmiana głośności muzyki i efektów

**Rozgrywka:**
Po uruchomieniu opcji nowej gry, na ekranie zostają wyświetlone grafiki:
- statyczny teren
- poruszający się w czasie księżyc oraz jego blask, który ukryty jest za innymi warstwami tak, aby podświetlał jedynie jezioro w tle
- grafika gracza, która zmienia położenie zależnie od klawiszy, jakimi posługuje się gracz
- grafiki przeciwników, które przemieszczają się w lewą stronę z prędkością określoną za pomocą zmiennych (ilość pikseli na sekundę)
- grafiki pocisków, które wyświetlają się w momencie wciśnięcia konkretnego przycisku klawiatury, które poruszają się z określoną prędkością w prawą stronę
- interfejs graficzny, pokazujący ilość zdrowia bohatera oraz możliwe użycie umiejętności

**System przeciwników:**
Po rozpoczęciu rozgrywki wygenerowane zostają 4 obiekty zombie i 4 obiekty ludzi, których początkowe położenie jest ustawiane losowo po prawej stronie po za ekranem.
Poruszają się w lewą stronę w kierunku gracza, a ich zdrowie ustawione jest na 50%. Gracz wchodzi w interakcje poprzez swoje umiejętności, zwiększając lub zmniejszając
poziom zdrowia przeciwnika. 

W momencie, gdy gracz "wyleczy" przeciwnika tj. aktualne zdrowie przeciwnika będzię równe 100% maksymalnego zdrowia przeciwnika,
zostanie zmieniona grafika danego przeciwnika, będzie odporny na kolizje z pociskami gracza i dalej będzie się poruszał w lewą stronę do momentu, gdy jego współrzędna X (poziom)
będzie po za widocznym ekranem. Wówczas jego położenie zostanie zmienione, tak aby ponownie był po za ekranem z prawej stronie, jego zdrowie ustawione będzie ponownie na 50% 
a jego grafika będzie tą samą co początkowo.

W momencie, gdy gracz "zniszczy" przeciwnika tj. aktualne zdrowie przeciwnika będzie mniejsze lub równe 0, grafika przeciwnika zostanie ukryta, będzie on odporny na kolizje
z pociskami, jego położenie zostanie ustawione ponownie po za ekranem z prawej strony, a jego zdrowie ustawione zostanie na 50%. W momencie przemieszczenia przeciwnika, jego
grafika zostaje ponownie pokazana.

Po upływnie określonego czasu rozgrywki zależnego od poziomu planszy, wszystkie grafiki przeciwników zostają ukryte i wyświetlona zostaje grafikia przeciwnika typu potwór,
która porusza się w lewą stronę ekranu, wystrzeliwując naprowadzane pociski w stronę gracza, które w momencie kolizji zmiejszają zdrowie bohatera.
Położenie pocisku przeciwnika opisywane jest za pomocą funkcji trygonometrycznej - w skrócie: pocisk porusza się po przeciwprostokątnej trójkąta, którego wierzchołkami są:
górna częśc grafiki przeciwnika, dolna część grafiki przeciwnika, górna częśc grafiki bohatera. Dzięki temu pociski "skręcają", gdy gracz zmienia położenie bohatera, zmuszając go do unikania pocisków.

**Animacje:** 
Animacje imitujące ruch postaci to podmiana grafiki postaci, która następuje zależnie od czasu rozgrywki:
za pomocą zmiennej liczona jest ilość wygenerowanych klatek rozgrywki (ilość wykonanych iteracji pętli while), dzięki czemu wyliczany jest czas jaki upłynął od rozpoczęcia rozgrywki.
Dzięki niemu następuje podmiana grafik postaci za pomocą operacji modulo w równych odstępach czasu.

**Kolizje:**
Kolizje następują wtedy, gdy obszar grafiki pocisku pokrywa się z obszarem grafiki postaci. Obszary te są wyliczane na podstawie aktualnych współrzędnych i jej wymiaru 
(od *współrzędna X* do *współrzędna X + szerokość grafiki*) oraz (od *współrzędna Y* do *współrzędna Y + wysokość grafiki*)

**Błędy i problemy**
Największym błędem jaki został napotkany podczas pisania tej aplikacji był fakt, iż gra "przyśpieszała" w momencie, gdy gracz poruszał kursorem podczas rozgrywki.
Położenie kursora myszy jest również opisywane za pomocą współrzędnych X oraz Y i gdy zmiana tych wartości była zbyt szybka, wszystkie grafiki przeciwników poruszały się szybciej (ich położenie, a co za tym idzie prędkość
ruchu zależna jest od ilości wygenerowanych klatek). W związku z tym, że zmiana położenia kursora myszy była większa niż 60 razy na sekundę (ilość wyświetlanych klatek na sekundę podczas rozgrywki - ilość iteracji pętli)
ilość wygenerowanych klatek rosła szybciej niż 60 raz na sekundę co powodowało do szybszej zmiany położenia przeciwników. Problem mógłby zostać rozwiązanych poprzez deinstalację myszy podczas rozgrywkia, ale ta powodowała
wyłączenie myszy po zakończeniu poziomu, uniemożliwiając graczowi kontynuację gry. Rozwiązano ten problem ustalając pozycję kursora myszy w punkcie zerowym układu współrzędnych 
(0,0) - lewy górny róg ekranu co każdą iterację pętli, unimożliwiając graczowi zbyt szybką zmianę położenia kurosra.

Innym błędem, którego nie rozwiązano jest problem z uruchomieniem aplikacji na niektórych komputerach, które nie posiadają odpowiednich bibliotek dll.



